#pragma once

#include "utl.hpp"
#include "section_defs.h"

#include <list>
#include <map>

namespace MK1_MAP {

typedef u32_t b_index_t;

struct Bone {
    std::string name;
    Vector3f position;
    b_index_t parent;
    std::vector<b_index_t> children;

    bool operator==(Bone const& b) const {
        return b.name == name && b.position == position;
    }
};

class Frame {
    friend class Animator;

    int frame_id;
    std::map<b_index_t, Matrix4f> transforms;

public:
    Frame(int frame_id);

    int getFrameID() const;

    std::map<b_index_t, Matrix4f> const& getTransforms() const;
};

class Animator {
    friend class Model;

    std::vector<Bone> bones;
    std::map<int, Frame> frames;

    Animator();

protected:
    void addFrameData(int frame_id, b_index_t b, Matrix4f const& t);

public:
    ~Animator();

    static Animator *importFromFile(std::string filename);
    void exportToFile(std::string filename);

    std::vector<Bone> const& getBones() const;
    std::map<int, Frame> const& getFrames() const;
};

} /* MK1_MAP */
