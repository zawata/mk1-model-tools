#pragma once

#include "utl.hpp"
#include "version.hpp"

#include <unordered_map>

extern std::unordered_map<std::string, std::string> alias_map;

namespace MK1_MAP {

const char *get_ver_str();
int get_ver();

void load_alias_list(std::string file);

} /* MK1_MAP */

void file_to_buffer(std::string filename, std::vector<u8_t> &filebuffer);
void buffer_to_file(std::vector<u8_t> const& filebuffer, std::string filename);

#include "mdl.hpp"
#include "anm.hpp"
