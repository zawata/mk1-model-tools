#pragma once

#include "ints.h"

#ifdef __cplusplus
extern "C" {
#endif

//simplify pad data
#define pad( n, l ) u8_t __pad_##n[l]

/*****************
 * Model Objects *
 *****************/

/**
 * Model Header Data
 **/
#pragma pack(push, 1)
typedef struct {
    u32_t    magic;                 // 0x00 // 0x324C444D
    u16_t    matrix_cnt;            // 0x04 // maybe
    u16_t    component_count;       // 0x06
    u16_t    sec_2_block_count;     // 0x08
    u16_t    joint_count;           // 0x0A
    off32_t  component_desc_offset; // 0x0C
    off32_t  sec_2_offset;          // 0x10
    off32_t  joint_data_offset;     // 0x14
//  u32_t    __                     // 0x18 // seemingly unused
//  u32_t    __                     // 0x1C // seemingly unused
    pad(1, 0x8);
    float    bb_1[4];               // 0x20 //bounding box point 1
    float    bb_2[4];               // 0x30 //bounding box point 2
    u32_t    dict_entries_cnt;      // 0x40
    off32_t  dict_offset;           // 0x44
//  u8_t[4]  __                     // 0x48 // seemingly unused
//  u32_t    __                     // 0x4C // seemingly unused
    pad(2, 0x8);
    u32_t    creation_date;         // 0x50
    off32_t  orig_file_str;         // 0x54
//  u32_t    __                     // 0x58 // seemingly unused
//  u16_t    __                     // 0x5C // seemingly unused
//  u16_t    __                     // 0x5E // seemingly unused
//  u32_t    __                     // 0x60 // seemingly unused
//  u32_t[3] __                     // 0x64
    pad(3, 0x18 );
} mdl_header_t;
#pragma pack(pop)
#define MDL_HEADER_SZ 0x70
static_assert( sizeof( mdl_header_t ) == MDL_HEADER_SZ);

/**
 * Model Component Descriptor
 **/
#pragma pack(push, 1)
typedef struct {
    float    bb_1[4];               // 0x00 // bounding box point 1
    float    bb_2[4];               // 0x10 // bounding box point 2
    float    joint[4];              // 0x20 // the joint that affects this component??
    off32_t  component_name_offset; // 0x30
    off32_t  addl_str_offset;       // 0x34
//  u32_t    __                     // 0x38 // seemingly unused
    pad(1, 0x4);
    u32_t    vbone_count;           // 0x3C vbone_cnt? possible values, 0,1,2
    u16_t    unknown_count;         // 0x40 // important...apparently
    u16_t    mesh_count;            // 0x42
    off32_t  mesh_desc_offset;      // 0x44
    pad(2, 0x4);
    u32_t    misc_ptr;              // 0x4C // pointer to malloc'd space length 2 or num_bones + 1 depending on the number at 0x3c
} component_descriptor_t;
#pragma pack(pop)
#define COMPONENT_DESCRIPTOR_SZ 0x50
static_assert( sizeof( component_descriptor_t ) == COMPONENT_DESCRIPTOR_SZ);

/**
 * Model Component Mesh Descriptor
 **/
#pragma pack(push, 1)
typedef struct {
    off32_t  texture_name_offset;   //0x00 // changed to pointer of some cached texture object
    off32_t  strip_list_offset;     //0x04
    pad(1, 0x4);                    //0x08 // CRC??
    u32_t    strip_list_count;      //0x0C
} mesh_descriptor_t;
#pragma pack(pop)
#define MESH_DESCRIPTOR_SZ 0x10
static_assert( sizeof( mesh_descriptor_t ) == MESH_DESCRIPTOR_SZ);

/**
 * Section 2 Definitions
 **/
#pragma pack(push, 1)
typedef struct {
//  vector4f
    pad(1, 0x10);
    off32_t  string_offset;         //0x10
    u32_t    uk_count;              //0x14
//  f32_t    __                     //0x18
//  f32_t    __                     //0x1C
    pad(2, 0x8);
} sec2_block_t;
#pragma pack(pop)
#define SEC_2_BLOCK_SZ 0x20
static_assert( sizeof( sec2_block_t ) == SEC_2_BLOCK_SZ);


/*********************
 * Animation Objects *
 *********************/

/**
 * Animation Header
 **/
#pragma pack(push, 1)
typedef struct {
    u32_t    magic;                 // 0x00 // 0x4D494E41
    u32_t    frame_count;           // 0x04
    u32_t    bone_desc_count;       // 0x08
    off32_t  bone_desc_offset;      // 0x0C
    u32_t    sec2_count;            // 0x10
    off32_t  sec2_offset;           // 0x14
    u32_t    transform_comp_count;  // 0x18
    off32_t  transform_comp_offset; // 0x1C
    u32_t    frame_transform_count; // 0x20
    off32_t  frame_transform_offset;// 0x24
//  u8_t[4]  __                     // 0x28
//  u32_t    __                     // 0x2C
    pad(2, 0x8);
} anm_header_t;
#pragma pack(pop)
#define ANM_HEADER_SZ 0x30
static_assert( sizeof( anm_header_t ) == ANM_HEADER_SZ);

/**
 * Bone Descriptor
 **/
#pragma pack(push, 1)
typedef struct {
    float    joint_pos[4];          //0x00
    off32_t  joint_name_offset;     //0x10
    s32_t    parent_id;             //0x14
    u32_t    data_count;            //0x18
    off32_t  data_offset;           //0x1C
} anm_bone_descriptor_t;
#pragma pack(pop)
#define ANM_BONE_DESCRIPTOR_SZ 0x20
static_assert( sizeof( anm_bone_descriptor_t ) == ANM_BONE_DESCRIPTOR_SZ);

#ifdef __cplusplus
}
#endif
