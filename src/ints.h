#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>

/**
 * Int Definitions
 *  Redefined to be shorter.
 * add '_t' to the types because my editor colors them differently
 **/
typedef uint8_t     u8_t  ;
typedef uint16_t    u16_t ;
typedef uint32_t    u32_t ;
typedef uint64_t    u64_t ;
typedef int8_t      s8_t  ;
typedef int16_t     s16_t ;
typedef int32_t     s32_t ;
typedef int64_t     s64_t ;

typedef float       f32_t;
typedef double      d64_t;
typedef long double d128_t;

typedef u32_t       off32_t;

#ifdef __cplusplus
}
#endif