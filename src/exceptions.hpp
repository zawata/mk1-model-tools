#pragma once

#include <exception>
#include <string>

/**
 * Simplified Exception Class Declaration Macro
 **/
#define build_exception(NAME) struct NAME ## Exception : public std::exception { \
    explicit NAME ## Exception(std::string s) : msg(s) {} \
    virtual const char *what() const noexcept { return msg.c_str(); } \
private: \
    std::string msg; \
}

/**
 * Exception Class Declarations
 **/
build_exception(FileNotFound);
build_exception(InvalidFileName);
build_exception(OutOfRange);
