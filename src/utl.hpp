#pragma once

#include <cstddef>
#include <fstream>
#include <iostream>
#include <vector>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "ints.h"

using Eigen::Vector2f;
using Eigen::Vector3f;
using Eigen::Vector4f;
using Eigen::Matrix4f;
using Eigen::Quaternionf;
typedef Eigen::Transform<float, 3, Eigen::Affine> Transformf;

// Binary data buffer
typedef std::vector<u8_t> bin_buffer_t;

/**
 * Vector increase size
 **/
template<typename T>
static void vec_inc_size(std::vector<T> &vec, size_t size) {
    vec.resize(vec.size() + size);
}

/**
 * Size of vector data
 **/
template<typename T>
static size_t vec_sizeof(std::vector<T> const& vec) {
    return vec.size() * sizeof(T);
}

/**
 * Size of vector data
 * String Specialization
 **/
static size_t vec_sizeof(std::vector<std::string> const& vec) {
    size_t size = 0;
    for(auto const&s : vec) {
        size += s.size() + 1;
    }

    return size;
}

/**
 * get pointer to location in vector data
 **/
static u8_t const* vec_get_ptr(bin_buffer_t const& vec, size_t data_loc) {
    return vec.data() + data_loc;
}

/**
 * Vector append buffer data
 **/
static void vec_append_buff(bin_buffer_t &vec, void const* data, size_t data_size) {
    size_t sz = vec.size();
    vec_inc_size(vec, data_size);
    memmove(vec.data() + sz, data, data_size);
}

/**
 * Vector append Vector data
 **/
template<typename T2>
static void vec_append_vec(bin_buffer_t &dst, std::vector<T2> const& src) {
    static_assert(std::is_fundamental<T2>::value);
    vec_append_buff(dst, src.data(), vec_sizeof(src));
}

/**
 * Vector append String Data
 **/
static void vec_append_str(bin_buffer_t &dst, std::string src) {
    vec_append_buff(dst, src.data(), src.size() + 1);
}
