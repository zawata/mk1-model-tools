#pragma once

#include "anm.hpp"
#include "utl.hpp"

#include "section_defs.h"

#include <list>

namespace MK1_MAP {

typedef u32_t v_index_t;

struct Vertex {
    Vector3f position, normal;
    Vector4f color;
    Vector2f uv_coordinates;
    float weight;
    int bone1, bone2;

    bool operator==(Vertex const& v) const {
        return (
            this->position == v.position &&
            this->normal == v.normal &&
            this->color == v.color &&
            this->uv_coordinates == v.uv_coordinates
        );
    }
};

struct Face {
    v_index_t v1,v2,v3;
};

class Mesh {
    friend class Component;
private:
    std::string texture_name;

    std::vector<Face> face_list;
    std::vector<Vertex> vertex_list;

    void import_from_file(std::vector<u8_t> const& file, mesh_descriptor_t const& m);
    void export_to_buffer(std::vector<u8_t> &buff, u32_t &strip_list_count) const;

    inline
    bool verify_face(Face const &f);

    inline
    bool verify_vertex(Vertex const& f);

    Mesh();

public:
    Mesh(std::string texture);
    ~Mesh();

    std::string getTextureName() const;
    std::vector<Face> const& getFaces() const;
    std::vector<Vertex> const& getVertices() const;
    std::list<std::vector<v_index_t>> getTriangleStrips() const;

    bool addFace(Face const& f);
    bool addVertex(Vertex const& v);

    bool addFaces(std::vector<Face> const& f);
    bool addVertices(std::vector<Vertex> const& v);

    void clear();
};

class Component {
    friend class Model;

    std::string name;

    std::list<Mesh> meshes;

    void import_from_file(std::vector<u8_t> const& file, component_descriptor_t const& c);
    void export_to_buffer(std::vector<u8_t> &buff, std::vector<std::string> &dict, std::vector<mesh_descriptor_t> &desc_list) const;

public:
    Component();
    Component(std::string name);
    ~Component();

    std::string getComponentName() const;
    std::list<Mesh> const& getMeshes() const;

    void addMesh(Mesh const& me);
    void addMeshes(std::list<Mesh> const& m);

    void clear();
};

class Model {
    std::string name;
    Animator anim;

    std::list<Component> components;
    std::vector<Vector3f> joints;

public:
    Model();
    Model(std::string model_name);
    ~Model();

    static Model *importFromFile(std::string model_name);
    void exportToFile(std::string file_name);

    std::string getName() const;
    std::string getFilename() const;

    Animator const& getAnimator() const;
    void setAnimator(Animator const& anim);

    std::list<Component> const& getComponents() const;
    void addComponent(Component const& c);

    //void clear();
};

} /* MK1_MAP */
