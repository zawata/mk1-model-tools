#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>

#include <tuple>
#include <type_traits>

#include "exceptions.hpp"
#include "mdl.hpp"
#include "mk1map.hpp"

namespace py = pybind11;

/**
 * Further simplified Exception Macro
 **/
#define exception(m, NAME) py::register_exception<NAME ## Exception>(m, #NAME"Error")

template<class T>
std::string to_string(T const& t) {
    static_assert(std::is_convertible<T, std::string>::value);
    return std::string(t);
}

template<typename T>
struct ItemIterator {
    std::list<T> l;
    typename std::list<T>::const_iterator it;

    ItemIterator(std::list<T> const& in_l): l(in_l), it(l.cbegin()) {}

    T next() {
        if (it == l.cend())
            throw py::stop_iteration();
        return *it++;
    }
};

/**
 * Python Module Declaration
 **/
PYBIND11_MODULE(pymk1map, mod) {

    /**
     * Initialization
     **/

    /**
     * Exceptions
     **/
    exception(mod, FileNotFound);
    exception(mod, InvalidFileName);
    exception(mod, OutOfRange);

    using py::self;
    using namespace MK1_MAP;

    mod.def("get_ver", &get_ver_str);
    mod.def("load_alias_list", &load_alias_list);

    /**
     * Model Definitions
     **/

    py::class_<Vertex>(mod, "Vertex")
        .def(py::init())
        .def_readwrite("position", &Vertex::position)
        .def_readwrite("normal", &Vertex::normal)
        .def_readwrite("color", &Vertex::color)
        .def_readwrite("uv_coord", &Vertex::uv_coordinates)
        .def_readwrite("bone_weight", &Vertex::weight)
        // .def_readwrite("bone1", &Vertex::bone1)
        // .def_readwrite("bone2", &Vertex::bone2)
        .def(self == self);

    using FaceIterator = ItemIterator<v_index_t>;

    auto face_cls =
    py::class_<Face>(mod, "Face")
        .def(py::init([](std::tuple<v_index_t, v_index_t, v_index_t> t){
            Face f;
            std::tie(f.v1, f.v2, f.v3) = t;
            return std::unique_ptr<Face>(new Face(f));
        }))
        .def_readwrite("v1", &Face::v1)
        .def_readwrite("v2", &Face::v2)
        .def_readwrite("v3", &Face::v3)
        // .def("as_tuple", [](Face const& self){ return std::make_tuple(self.v1, self.v2, self.v3); })
        .def("__iter__", [](Face const& self){
            return FaceIterator({self.v1, self.v2, self.v3});
        });

        //use a custom iterator class since the contents of the face struct are not in a collection
        py::class_<FaceIterator>(face_cls, "Iterator")
            .def("__iter__", [](FaceIterator &it) -> FaceIterator& { return it; })
            .def("__next__", &FaceIterator::next);

    py::class_<Mesh>(mod, "Mesh")
        .def(py::init<std::string>())
        .def("getTextureName", &Mesh::getTextureName)
        .def("getFaces", &Mesh::getFaces)
        .def("getVertices", &Mesh::getVertices)
        .def("getTriangleStrips", &Mesh::getTriangleStrips)
        .def("addFace", &Mesh::addFace)
        .def("addVertex", &Mesh::addVertex)
        .def("addFaces", &Mesh::addFaces)
        .def("addVertices", &Mesh::addVertices)
        .def("clear", &Mesh::clear);

    py::class_<Component>(mod, "Component")
        .def(py::init())
        .def(py::init<std::string>())
        .def("getComponentName", &Component::getComponentName)
        .def("getMeshes", &Component::getMeshes)
        .def("addMesh", &Component::addMesh)
        .def("addMeshes", &Component::addMeshes)
        .def("clear", &Component::clear);

    py::class_<Model>(mod, "Model")
        .def(py::init())
        .def(py::init<std::string>())
        .def_static("importFromFile", &Model::importFromFile)
        .def("exportToFile", &Model::exportToFile)
        .def("getName", &Model::getName)
        .def("getFilename", &Model::getFilename)
        .def("getAnimator", &Model::getAnimator)
        .def("setAnimator", &Model::setAnimator)
        .def("getComponents", &Model::getComponents)
        .def("addComponent", &Model::addComponent);

    /**
     * Animation Definitions
     **/

    py::class_<Bone>(mod, "Bone")
        .def_readonly("name", &Bone::name)
        .def_readonly("position", &Bone::position)
        .def_readonly("parent", &Bone::parent)
        .def_readonly("children", &Bone::children)
        .def(self == self);

    py::class_<Frame>(mod, "Frame")
        .def("getFrameID", &Frame::getFrameID)
        .def("getTransforms", &Frame::getTransforms);

    py::class_<Animator>(mod, "Animator")
        .def("getBones", &Animator::getBones)
        .def("getFrames", &Animator::getFrames);
}
