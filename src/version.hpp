#pragma once

constexpr auto VERSION_STR = "3.0.0d";
constexpr auto VERSION = 0x03000;