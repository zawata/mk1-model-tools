#include <iostream>
#include <iterator>

#include <Eigen/Core>

#include "mk1map.hpp"

int main(int argc, char **argv) {
#if _WIN32
    MK1_MAP::load_alias_list("D:\\ty_Stuff\\mdl_proc\\mdl_mk1\\io-scene-mdl\\alias_list.txt");
#else
    MK1_MAP::load_alias_list("./io-scene-mdl/alias_list.txt");
#endif

    std::cout << "Merkury V1 Model and Animation Processer v" << MK1_MAP::get_ver_str() << std::endl;

    if (argc != 2) {
        std::cout << "No File Given" << std::endl;
        return -1;
    }

    // std::string s(argv[1]);

    std::string s("D:\\ty_Stuff\\output\\Act1_43_MattDucky");
    //std::string s("output");

    std::cout << s + ".mdl" << std::endl;
    auto m = MK1_MAP::Model::importFromFile(s + ".mdl");

    std::cout << "Model Name: " << m->getName() << std::endl;
    auto comp_list = m->getComponents();
    std::cout << "Components:" << std::endl;
    for(auto c : comp_list) {
        std::cout << "\t" << "Component Name:" << c.getComponentName() << std::endl;
        auto mesh_list = c.getMeshes();
        std::cout << "\t" << "Meshes:" << std::endl;
        for(auto m : mesh_list) {
            std::cout << "\t\t" << "Texture name:" << m.getTextureName() << std::endl;
            std::cout << "\t\t" << "Faces: " << std::endl;
            for(auto f : m.getFaces()) {
                std::cout << "\t\t\t" << "(" << f.v1 << ", " << f.v2 << ", " << f.v3 << ")" << std::endl;
            }
            std::cout << "\t\t" << "Vertices: " << std::endl;
            for(auto v : m.getVertices()) {
                Eigen::IOFormat f(4,0,",","","(",")","","");
                std::cout << "\t\t\t" << v.position.format(f) << ", " << v.normal.format(f) << std::endl;
            }
        }
    }

    m->exportToFile("output.mdl");

    //std::cout << s + ".anm" << std::endl;

    //auto a = m.getAnimator();
    //std::cout << "Animation Name: " << a.getName() << std::endl;
    //auto bone_list = a.getBones();
    //std::cout << "Bones:" << std::endl;
    //Eigen::IOFormat f(4, 0, ",", "", "(", ")", "", "");
    //for(auto b : bone_list) {
    //    std::cout << "\t" << "Bone Name: " << b->name << std::endl;
    //    std::cout << "\t" << "Parent: " << ((b->parent != nullptr) ? b->parent->name : "None") << std::endl;
    //    std::cout << "\t" << "location: " << b->position.format(f) << std::endl;
    //}

    //auto frame_list = a.getFrames();
    //std::cout << "Frames: " << std::endl;
    //for(auto f : frame_list) {
    //    std::cout << "\t" << "Frame ID: " << f.first << std::endl;
    //    for(auto t : f.second.getTransforms()) {
    //        std::shared_ptr<MK1_MAP::Bone> b;
    //        Transformf tr;
    //        std::tie(b,tr) = t;
    //        std::cout << "\t\t" << "Bone: " << b->name << std::endl;
    //        Eigen::IOFormat MatFmt(4, 0, ", ", "\n", "\t\t\t[", "]");
    //        std::cout << "\t\t" << "Transform: " << std::endl << tr.matrix().format(MatFmt) << std::endl;
    //    }
    //}

    return 0;
}