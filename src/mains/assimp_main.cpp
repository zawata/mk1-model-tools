#include <string>

#include <list>
#include <vector>

#include <assimp/Importer.hpp>
#include <assimp/Exporter.hpp>

#include <assimp/scene.h>
#include <assimp/mesh.h>
#include <assimp/postprocess.h>

#include "assimp/color4.h"
#include "assimp/material.h"
#include "assimp/matrix4x4.h"
#include "assimp/vector3.h"


#include "mdl.hpp"
#include "mk1map.hpp"

void get_global_matrix(const aiNode *node, aiMatrix4x4 & matrix ) {
    //set output matrix to identity;
    matrix = aiMatrix4x4();

    const aiNode *current_node = node;
    do {
        matrix *= node->mTransformation;
        node = node->mParent;
    } while(current_node);
}

void to_EigenMatrix(const aiMatrix4x4 &aiMatrix, Eigen::Matrix4f &eMatrix) {
    eMatrix.setIdentity();

    //theres probably a more efficient way to do this. but this way is easiest for now.
    eMatrix.row(0)[0] = aiMatrix[0][0];
    eMatrix.row(0)[1] = aiMatrix[0][1];
    eMatrix.row(0)[2] = aiMatrix[0][2];
    eMatrix.row(0)[3] = aiMatrix[0][3];

    eMatrix.row(1)[0] = aiMatrix[1][0];
    eMatrix.row(1)[1] = aiMatrix[1][1];
    eMatrix.row(1)[2] = aiMatrix[1][2];
    eMatrix.row(1)[3] = aiMatrix[1][3];

    eMatrix.row(2)[0] = aiMatrix[2][0];
    eMatrix.row(2)[1] = aiMatrix[2][1];
    eMatrix.row(2)[2] = aiMatrix[2][2];
    eMatrix.row(2)[3] = aiMatrix[2][3];

    eMatrix.row(3)[0] = aiMatrix[3][0];
    eMatrix.row(3)[1] = aiMatrix[3][1];
    eMatrix.row(3)[2] = aiMatrix[3][2];
    eMatrix.row(3)[3] = aiMatrix[3][3];
}

void convert_uv(const aiVector3D &aiVector, Eigen::Vector2f &eVector) {
    eVector = Eigen::Vector2f(
            aiVector.x,
            aiVector.y);
}

void convert_vector(const aiVector3D &aiVector, Eigen::Vector3f &eVector) {
    eVector = Eigen::Vector3f(
            aiVector.x,
            aiVector.y,
            aiVector.z);
}

void convert_color(const aiColor4D &aiColor, Eigen::Vector4f &eVector) {
    eVector = Eigen::Vector4f(
            aiColor.r,
            aiColor.g,
            aiColor.b,
            aiColor.a);
}

void get_texture() {

}

//constructs mesh
void build_mesh(const aiMesh *ai_mesh, MK1_MAP::Mesh &mk1_mesh) {
    mk1_mesh.clear();

    //build out vertices first
    for(unsigned i = 0; i < ai_mesh->mNumVertices; i++) {
        MK1_MAP::Vertex v;

        convert_vector(ai_mesh->mVertices[i], v.position);
        convert_vector(ai_mesh->mVertices[i], v.normal);

        //Check if we've got Vertex Color Channels as they are kind of rare
        if(ai_mesh->mColors[0]) {
            convert_color(ai_mesh->mColors[0][i], v.color);
        } else {
            //TODO: should this be black or white?
            v.color = {0,0,0,0};
        }

        //check if we have valid UV Channels.
        // I think this is enforced by the post-processing flags but I'm too lazy to check
        //also we may still need to handle the scenario where we don't have any textures.
        if(ai_mesh->mTextureCoords[0] && ai_mesh->mNumUVComponents[0] == 2) {
            convert_uv(ai_mesh->mTextureCoords[0][i], v.uv_coordinates);
        } else {
            v.uv_coordinates = {0,0};
        }

        //TODO:
        //bone data;
        v.weight = 0;

        mk1_mesh.addVertex(v);
    }

    //then build_out faces which are luckily much easier
    for(unsigned i = 0; i < ai_mesh->mNumFaces; i++) {
        MK1_MAP::Face face;

        // these should be guaranteed to be triangles since we enforce it during postprocessing
        face.v1 = ai_mesh->mFaces->mIndices[0];
        face.v2 = ai_mesh->mFaces->mIndices[1];
        face.v3 = ai_mesh->mFaces->mIndices[2];

        mk1_mesh.addFace(face);
    }
}

void import_gltf( const std::string pFile) {
    Assimp::Importer importer;

    u32_t import_flags =
            /* duplicate vertices make the MDL files larger */
            aiProcess_JoinIdenticalVertices |
            /* MDL only supports triangles */
            aiProcess_Triangulate |
            /* MDL requires Normals, Generate them if they aren't present */
            aiProcess_GenNormals | //TODO: smooth?
            /* split large meshes into a list of small ones */
            aiProcess_SplitLargeMeshes |
            /* this essentially disables the scenegraph functionality */
            /* TODO: revisit in the future */
            // aiProcess_PreTransformVertices |
            /* This just seems nice to have */
            aiProcess_FixInfacingNormals |
            /* sort primitives so we can filter out line and point primitives */
            aiProcess_SortByPType |
            /* get rid of tiny and malformed triangles */
            aiProcess_FindDegenerates |
            /* force UV style texture mapping */
            aiProcess_GenUVCoords |
            /* TODO: is this needed? */
            // aiProcess_GenUVCoords |
            0;

    //TODO: importer properties
    // located in config.h
    // set AI_CONFIG_PP_SBP_REMOVE = aiPrimitiveType_LINE | aiPrimitiveType_POINT

    const aiScene* scene = importer.ReadFile( pFile, import_flags);
    if(!scene || !(scene->mFlags | AI_SCENE_FLAGS_INCOMPLETE)) {
        // Throw
    }

    MK1_MAP::Model m = MK1_MAP::Model("");


    //Depth first search the node graph for all nodes with meshes.
    // any node that has at least 1 mesh will be added to the component list
    // each node stores a reference to it's parent so we can just store the node reference
    std::vector<struct aiNode *> component_list;

    std::list<struct aiNode *> node_list;
    node_list.push_back(scene->mRootNode);
    while(!node_list.empty()) {
        struct aiNode * node = node_list.front();

        if(node->mNumMeshes) {
            component_list.push_back(node);
        }

        for(unsigned i = 0; i < node->mNumChildren; i++) {
            node_list.push_back(node->mChildren[i]);
        }
    }

    for(auto node : component_list) {
        MK1_MAP::Component comp = MK1_MAP::Component(node->mName.C_Str());

        for(unsigned i = 0; i < node->mNumMeshes; i++) {
            const aiMesh *ai_mesh = scene->mMeshes[node->mMeshes[i]];
            const aiMaterial *mat = scene->mMaterials[ai_mesh->mMaterialIndex];

            /**
             * How Materials work:
             *
             * A material is comprised of 1-6 "texture stacks"
             * The types of textures stacks:
             *  - AMBIENT
             *  - DIFFUSE
             *  - EMISSIVE
             *  - REFLECTIVE
             *  - SPECULAR
             *  - TRANSPARENT
             *
             * DIFFUSE is the most noticable.
             *
             * Each Texture Stack is a base color followed by blending in any number of textures.
             *
             * MDL only supports a single texture per mesh so we need to make sure it's the "Base Texture"
             * for this mesh.
             *
             * I think to be easy we require that this diffuse stack so we can get the texture from it.
             *
             * If the DIFFUSE texture stack contains multiple texture then we've got a couple options:
             * 1. fail
             * 2. use the first texture
             * 3. use the texture with the highest "weight"
             * 4. ask the user
             */

            std::cout << "Mesh Name:" << std::endl;
            std::cout << "\t" << ai_mesh->mName.C_Str() << std::endl;
            std::cout << "Texture Counts: " << std::endl;
            std::cout << "\tBASE_COLOR: " << mat->GetTextureCount(aiTextureType_BASE_COLOR) << std::endl;
            std::cout << "\tNORMAL_CAMERA: " << mat->GetTextureCount(aiTextureType_NORMAL_CAMERA) << std::endl;
            std::cout << "\tEMISSION_COLOR: " << mat->GetTextureCount(aiTextureType_EMISSION_COLOR) << std::endl;
            std::cout << "\tMETALNESS: " << mat->GetTextureCount(aiTextureType_METALNESS) << std::endl;
            std::cout << "\tDIFFUSE_ROUGHNESS: " << mat->GetTextureCount(aiTextureType_DIFFUSE_ROUGHNESS) << std::endl;
            std::cout << "\tAMBIENT_OCCLUSION: " << mat->GetTextureCount(aiTextureType_AMBIENT_OCCLUSION) << std::endl;

            // int bc_tex_cnt = mat->GetTextureCount(aiTextureType_BASE_COLOR);
            // int tex_idx = -1;
            // if(bc_tex_cnt == 0) {
            //     std::cout << "Warning: Model has no Base Color textures." << std::endl;
            // }
            // else if(bc_tex_cnt == 1) {
            //         tex_idx = 0;
            // }
            // else {
            //     std::cout << "DIFFUSE Texture Stack has multiple textures but only 1 may be supplied. please choose:" << std::endl;
            //     for(int i = 0; i < bc_tex_cnt; i++) {
            //         printf("%02d. ", i);
            //         aiString tex_path;
            //         mat->GetTexture(aiTextureType_BASE_COLOR, i, &tex_path);
            //         std::cout << tex_path.C_Str() << std::endl;
            //     }
            // }

            //TODO: should we skip this mesh if it's material is not a base color texture?

            // std::cout << comp.getComponentName() << ": " << mat->

            // MK1_MAP::Mesh m = MK1_MAP::Mesh();
            // build_mesh(ai_mesh, m);

        }
    }

    return;
}

void import_mdl(std::string pFile) {

}

int main() {
    import_gltf("./gltf_test/Lantern/Lantern.gltf");

    return 0;
}