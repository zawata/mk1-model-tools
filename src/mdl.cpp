#include "mdl.hpp"
#include "mk1map.hpp"
#include "stripgen.hpp"

#include <cstring>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <tuple>
#include <type_traits>

#include "section_defs.h"
#include "exceptions.hpp"
#include "utl.hpp"


#define as_hex(u) std::hex << ((unsigned long long)u) << std::dec

/**
    The Lookup table that defines vertex color encoding:
      0,   1,   3,   5,   7,   9,  11,  13,  15,  17,  19,  21,  23,  25,  27,  29,
     31,  33,  35,  37,  39,  41,  43,  45,  47,  49,  51,  53,  55,  57,  59,  61,
     63,  65,  67,  69,  71,  73,  75,  77,  79,  81,  83,  85,  87,  89,  91,  93,
     95,  97,  99, 101, 103, 105, 107, 109, 111, 113, 115, 117, 119, 121, 123, 125,
    127, 129, 131, 133, 135, 137, 139, 141, 143, 145, 147, 149, 151, 153, 155, 157,
    159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179, 181, 183, 185, 187, 189,
    191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215, 217, 219, 221,
    223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251, 253,
    255,
      0,   2,   4,   6,   8,  10,  12,  14,  16,  18,  20,  22,  24,  26,  28,  30,
     32,  34,  36,  38,  40,  42,  44,  46,  48,  50,  52,  54,  56,  58,  60,  62,
     64,  66,  68,  70,  72,  74,  76,  78,  80,  82,  84,  86,  88,  90,  92,  94,
     96,  98, 100, 102, 104, 106, 108, 110, 112, 114, 116, 118, 120, 122, 124, 126,
    128, 130, 132, 134, 136, 138, 140, 142, 144, 146, 148, 150, 152, 154, 156, 158,
    160, 162, 164, 166, 168, 170, 172, 174, 176, 178, 180, 182, 184, 186, 188, 190,
    192, 194, 196, 198, 200, 202, 204, 206, 208, 210, 212, 214, 216, 218, 220, 222,
    224, 226, 228, 230, 232, 234, 236, 238, 240, 242, 244, 246, 248, 250, 252

    This table is generated with the code extracted from the game's assembly:

    (((short)x * 0xFF) >> 7) & 0xFF

    The issue with this encoding scheme is that both 0 and 0x81 map to 0 output making
    it non-reversible. This isn't something I can overcome except to exclude 0x81 as a
    possible output.
 **/

float u8_to_color(u8_t c) {
    if(c == 0) return 0.0f;

    u8_t u = ((c <= 0x80) ? (2*c)-1 : (c-1)*2);
    return (float)u/255.0f;
}

u8_t color_to_u8(float c) {
    if( c == 0.0f) return 0;

    u8_t u = c * 255.0f;
    if(u & 1) {
        return ((u+1)>>1);
    }
    else {
        return (((u>>1)+1) | 0x80);
    }
}

Vector3f computed_normal(Vertex const& v1, Vertex const& v2, Vertex const& v3) {
    return (v3.position - v1.position).cross(v2.position - v1.position).normalized();
}

//static Vector3f convert_position(float x, float y, float z) {
//    return Vector3f{
//        x,
//        z * -1,
//        y,
//    };
//}

Eigen::IOFormat pos_form(4,0,",","","(",")","","");

using namespace MK1_MAP;

/**
 * Mesh Implmentation
 **/
Mesh::Mesh() {}
Mesh::Mesh(std::string texture) : texture_name(texture) {}

Mesh::~Mesh() {}

v_index_t insert_vertex(std::vector<Vertex> &l, Vertex const& v) {
    for(auto it = l.begin(); it != l.end(); it++) {
        auto &l_v = *it;

        if(v == l_v) {
            return std::distance(l.begin(), it);
        }
    }
    l.push_back(v);
    return l.size() - 1;
}

/**
 * Import MK1 Model data from buffer
 *
 * Scope: Class Private
 **/
void Mesh::import_from_file(std::vector<u8_t> const& file, mesh_descriptor_t const& m) {
    this->vertex_list.clear();
    this->face_list.clear();

    this->texture_name = std::string((char *)vec_get_ptr(file, m.texture_name_offset));

    //convert texture_name to lowercase
    std::transform(texture_name.begin(), texture_name.end(), texture_name.begin(),
        [](unsigned char c){ return std::tolower(c); });

    if(alias_map.count(this->texture_name)) {
        texture_name = alias_map[texture_name];
    }

    u32_t data_off = m.strip_list_offset;
    for(u32_t triangle_strip_num = 0; triangle_strip_num < m.strip_list_count; triangle_strip_num++) {
        int vertex_count = *(u32_t *)vec_get_ptr(file, data_off + 0xC),
            chnk_len     = (0x1c * vertex_count) + 0x40;

        u32_t float_data_off = data_off + 0x34,
              char_data_off  = float_data_off + ( vertex_count * 12 ) + 0x4,
              short_data_off = char_data_off  + ( vertex_count *  4 ) + 0x4,
              color_data_off = short_data_off + ( vertex_count *  8 ) + 0x4;

        //std::cout << "strip: ";

        v_index_t face_idxs[3];
        for(int vertex_num = 0; vertex_num < vertex_count; vertex_num++) {
            int arr_loc = vertex_num % 3;

            float float_data[3];
            std::memcpy(float_data, vec_get_ptr(file, float_data_off), sizeof(float_data));
            float_data_off += sizeof(float_data);

            s8_t char_data[4];
            std::memcpy(char_data, vec_get_ptr(file, char_data_off), sizeof(char_data));
            char_data_off += sizeof(char_data);

            s16_t short_data[4];
            std::memcpy(short_data, vec_get_ptr(file, short_data_off), sizeof(short_data));
            short_data_off += sizeof(short_data);

            u8_t color_data[4];
            std::memcpy(color_data, vec_get_ptr(file, color_data_off), sizeof(color_data));
            color_data_off += sizeof(color_data);

            auto v = Vertex();
            v.position = {
                float_data[0],
                float_data[1],
                float_data[2] };
            v.normal = {
                (f32_t)(char_data[0]) / 127.0f,
                (f32_t)(char_data[1]) / 127.0f,
                (f32_t)(char_data[2]) / 127.0f };
            v.color = {
                u8_to_color(color_data[0]),
                u8_to_color(color_data[1]),
                u8_to_color(color_data[2]),
                u8_to_color(color_data[3]) };
            v.uv_coordinates = {
                (f32_t)(short_data[0]) / 4096.0f,
                (f32_t)(short_data[1]) / 4096.0f };
            v.bone1 = ((u16_t)short_data[3] >> 2) - 1;
            v.bone2 = ((u8_t)char_data[3] >> 1) - 1;
            v.weight = (f32_t)((u16_t)short_data[2]) / 4096.0f;

            v.normal.normalize();

            v_index_t v_index = insert_vertex(this->vertex_list, v);

            //std::cout << v_index << ",";

            face_idxs[arr_loc] = v_index;
            if( 1 < vertex_num ) {
                Vector3f computed_norm = computed_normal(
                        this->vertex_list[face_idxs[0]],
                        this->vertex_list[face_idxs[1]],
                        this->vertex_list[face_idxs[2]]);

                Vector3f actual_norm = (this->vertex_list[face_idxs[0]].normal + this->vertex_list[face_idxs[1]].normal + this->vertex_list[face_idxs[2]].normal).normalized();

                float norm_dir = computed_norm.dot(actual_norm);
                if( norm_dir >= 0.0f ) {
                    this->face_list.push_back({
                        face_idxs[0],
                        face_idxs[1],
                        face_idxs[2]});
                }
                else {
                    this->face_list.push_back({
                        face_idxs[2],
                        face_idxs[1],
                        face_idxs[0]});
                }
            }
        }
        //std::cout << std::endl;
        data_off += chnk_len;
    }
}

/**
 * Export MK1 Model data to buffer
 *
 * Scope: Class Private
 **/
void Mesh::export_to_buffer(std::vector<u8_t> &buff, u32_t &strip_list_count) const {
    std::list<std::vector<v_index_t>> triangle_strips = this->getTriangleStrips();

    strip_list_count = triangle_strips.size();

    bool first = true;
    for(auto const& strip : triangle_strips) {
        std::vector<float> float_buff;
        std::vector<s8_t> char_buff;
        std::vector<s16_t> short_buff;
        std::vector<u8_t> color_buff;

        for(auto const& v_idx : strip) {
            auto const& v = vertex_list.at(v_idx);

            float_buff.push_back(v.position[0]);
            float_buff.push_back(v.position[1]);
            float_buff.push_back(v.position[2]);

            char_buff.push_back((s8_t)(v.normal[0] * 127.0f));
            char_buff.push_back((s8_t)(v.normal[1] * 127.0f));
            char_buff.push_back((s8_t)(v.normal[2] * 127.0f));
            char_buff.push_back(0);
            // char_buff.push_back((s16_t)((v.bone2 + 1) << 1));

            short_buff.push_back((s16_t)(v.uv_coordinates[0] * 4096.0f));
            short_buff.push_back((s16_t)(v.uv_coordinates[1] * 4096.0f)); //TODO: normalize
            short_buff.push_back((s16_t)(v.weight * 4096.0f));
            short_buff.push_back(0);
            // short_buff.push_back((s16_t)((v.bone1 + 1) << 2));

            color_buff.push_back(color_to_u8(v.color[0]));
            color_buff.push_back(color_to_u8(v.color[1]));
            color_buff.push_back(color_to_u8(v.color[2]));
            color_buff.push_back(color_to_u8(v.color[3]));
        }

        std::vector<u8_t> strip_buffer;
        //append start of triangle strip header
        if(first){
            vec_inc_size(strip_buffer, 0xC);
        }
        else {
            //on strips that aren't the first one, theres a predicable header that we can use.
            // this is unneccessary for model validity but it helps for debugging
            uint8_t arr[] = {
                0xFF, 0xFF, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x14,
                0x00, 0x00, 0x00, 0x00
            };
            vec_append_buff(strip_buffer, arr, 12);
        }
        first = false;

        //append size
        u32_t strip_size = strip.size();
        vec_append_buff(strip_buffer, &strip_size, 4);

        //append rest of triangle strip header
        vec_inc_size(strip_buffer, 0x20);

        //append tag and float buffer
        vec_inc_size(strip_buffer, 4);
        vec_append_vec(strip_buffer, float_buff);

        //append tag and char buffer
        vec_inc_size(strip_buffer, 4);
        vec_append_vec(strip_buffer, char_buff);

        //append tag and short buffer
        vec_inc_size(strip_buffer, 4);
        vec_append_vec(strip_buffer, short_buff);

        //append tag and color buffer
        vec_inc_size(strip_buffer, 4);
        vec_append_vec(strip_buffer, color_buff);

        //append strip_buffer to ouput buffer
        vec_append_vec(buff, strip_buffer);
    }

    //all mesh lists contain 1 empty strip decriptor at the very end of their data...apparently
    // this is unneccessary for model validity but it helps for debugging
    uint8_t arr[] = {
        0xFF, 0xFF, 0x00, 0x01,
        0x00, 0x00, 0x00, 0x14,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,

        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00
    };
    vec_append_buff(buff, arr, 0x20);

}

inline
bool Mesh::verify_face(Face const& f) {
    return std::max({f.v1, f.v2, f.v3}) < this->vertex_list.size();
}

inline
bool Mesh::verify_vertex(Vertex const& f) {
    //TODO: ??
    return true;
}


std::string Mesh::getTextureName() const {
    return this->texture_name;
}

std::vector<Face> const& Mesh::getFaces() const {
    return this->face_list;
}

std::vector<Vertex> const& Mesh::getVertices() const {
    return this->vertex_list;
}

std::list<std::vector<v_index_t>> Mesh::getTriangleStrips() const {
    std::list<std::vector<v_index_t>> triangle_strips;

    auto sg = strip_generator(this->face_list);
    sg.generate_strips(triangle_strips);

    return triangle_strips;
}

bool Mesh::addFace(Face const& f) {
    this->face_list.push_back(f);

    return true;
}

bool Mesh::addVertex(Vertex const& v) {
    if(verify_vertex(v)) {
        this->vertex_list.push_back(v);
        return true;
    }
    return false;
}

bool Mesh::addFaces(std::vector<Face> const& f) {
    for(auto const& face : f) {
        if(!addFace(face))
            return false;
    }
    return true;
}

bool Mesh::addVertices(std::vector<Vertex> const& v) {
    for(auto const& vertex : v) {
        if(!addVertex(vertex))
            return false;
    }
    return true;
}

void Mesh::clear() {
    this->vertex_list.clear();
    this->face_list.clear();
}

/**
 * Component Implementation
 **/
Component::Component() : name("") {}
Component::Component(std::string name) : name(name) {}

Component::~Component() {}

void Component::import_from_file(std::vector<u8_t> const& file, component_descriptor_t const& c) {
    this->name = std::string((char*)vec_get_ptr(file, c.component_name_offset));
    for(int i = 0; i < c.mesh_count; i++) {
        off32_t mesh_desc_offset = c.mesh_desc_offset + (i * MESH_DESCRIPTOR_SZ);
        mesh_descriptor_t *m = (mesh_descriptor_t *)vec_get_ptr(file, mesh_desc_offset);

        Mesh mesh;
        mesh.import_from_file(file, *m);

        this->meshes.push_back(mesh);
    }
}

void Component::export_to_buffer(std::vector<u8_t> &buff, std::vector<std::string> &dict, std::vector<mesh_descriptor_t> &desc_list) const {
    buff.clear();

    for(auto const& mesh : this->meshes) {
        std::vector<u8_t> mesh_buffer;
        mesh_descriptor_t m_desc;
        m_desc.texture_name_offset = vec_sizeof(dict);
        m_desc.strip_list_offset = (u32_t)buff.size();

        mesh.export_to_buffer(mesh_buffer, m_desc.strip_list_count);

        vec_append_vec(buff, mesh_buffer);
        dict.push_back(mesh.texture_name);
        desc_list.push_back(m_desc);
    }
}

std::string Component::getComponentName() const {
    return this->name;
}

std::list<Mesh> const& Component::getMeshes() const {
    return this->meshes;
}

void Component::addMesh(Mesh const& v) {
    this->meshes.push_back(v);
}

void Component::addMeshes(std::list<Mesh> const& v) {
    for(auto const& mesh : v) {
        addMesh(mesh);
    }
}

void Component::clear() {
    this->meshes.clear();
}


/**
 * Model Implementation
 **/
Model::Model() : name("") {}
Model::Model(std::string name) : name(name) {}

Model::~Model() {}

Model *Model::importFromFile(std::string model_name) {
    Model model(model_name);
    std::vector<u8_t> file;
    file_to_buffer(model_name, file);

    if (alias_map.size() == 0) {
        std::cout << "Alias List not loaded. Aliased Texture Names will not be converted." << std::endl;
    }

    mdl_header_t *h = (mdl_header_t *)file.data();
    for(int i = 0; i < h->component_count; i++) {
        off32_t component_offset = h->component_desc_offset + (i * COMPONENT_DESCRIPTOR_SZ);

        Component comp;
        component_descriptor_t *c = (component_descriptor_t *)vec_get_ptr(file, component_offset);
        comp.import_from_file(file, *c);

        model.components.push_back(comp);
    }

    for(int i = 0; i < h->joint_count; i++) {
        off32_t joint_desc_offset = h->joint_data_offset + (i * 0x10);

        float *joint_data_ptr = (float *)vec_get_ptr(file, joint_desc_offset);

        model.joints.push_back({
            joint_data_ptr[0],
            joint_data_ptr[1],
            joint_data_ptr[2],
        });
    }

    return new Model(model);
}

void Model::exportToFile(std::string file_name) {
    struct component_data_t {
        component_descriptor_t component_desc;
        std::vector<mesh_descriptor_t> mesh_descriptors;
        std::vector<u8_t> comp_buffer;
    };

    std::vector<component_data_t> comp_data_list;
    std::vector<std::string> dictionary;
    size_t comp_data_sz = 0;
    for(auto const& component : components) {
        component_data_t comp_data;

        comp_data.component_desc.mesh_desc_offset = 0;
        comp_data.component_desc.vbone_count = 0;

        comp_data.component_desc.component_name_offset = vec_sizeof(dictionary);
        dictionary.push_back(component.name);
        comp_data.component_desc.addl_str_offset = vec_sizeof(dictionary);
        dictionary.push_back(std::string(""));

        component.export_to_buffer(comp_data.comp_buffer, dictionary, comp_data.mesh_descriptors);

        comp_data_list.push_back(comp_data);

        comp_data_sz += sizeof(component_descriptor_t);
        comp_data_sz += sizeof(mesh_descriptor_t) * comp_data.mesh_descriptors.size();
        comp_data_sz += comp_data.comp_buffer.size();
    }

    //TODO: section 2 exports

    size_t joint_data_offset = sizeof(mdl_header_t) + comp_data_sz;
    std::vector<u8_t> joint_data;
    for(auto const& joint : joints) {
        vec_append_buff(joint_data, joint.data(), 12);
        vec_inc_size(joint_data, 4);
    }

    size_t dict_offset = joint_data_offset + joint_data.size();

    mdl_header_t mdl_hdr;
    mdl_hdr.magic = 0x324C444D;
    mdl_hdr.matrix_cnt = joints.size() + 1;
    mdl_hdr.component_count = (u16_t)this->components.size();
    mdl_hdr.component_desc_offset = sizeof(mdl_header_t);
    mdl_hdr.sec_2_block_count = 0;
    mdl_hdr.sec_2_offset = 0;
    mdl_hdr.joint_count = joints.size();
    mdl_hdr.joint_data_offset = joint_data_offset;
    mdl_hdr.dict_entries_cnt = dictionary.size();
    mdl_hdr.dict_offset = (off32_t)dict_offset;
    // .creation_date = get_date();

    std::vector<u8_t> file_buffer;
    vec_append_buff(file_buffer, &mdl_hdr, sizeof(mdl_header_t));

    u32_t mesh_desc_offset =
          sizeof(mdl_header_t)
        + comp_data_list.size() * sizeof(component_descriptor_t)
        // + sec_2_header_list.size() * sizeof(sec2_block_t)
        ;

    // Append Component Descriptors
    for(auto &comp : comp_data_list) {
        for(auto &desc : comp.mesh_descriptors) {
            desc.texture_name_offset += dict_offset;
        }

        comp.component_desc.component_name_offset += dict_offset;
        comp.component_desc.addl_str_offset += dict_offset;
        comp.component_desc.mesh_desc_offset += mesh_desc_offset;
        comp.component_desc.mesh_count = comp.mesh_descriptors.size();

        vec_append_buff(file_buffer, &comp.component_desc, sizeof(component_descriptor_t));

        mesh_desc_offset += comp.mesh_descriptors.size() * sizeof(mesh_descriptor_t);
    }

    //Append Mesh Descriptors
    u32_t strip_offset = mesh_desc_offset;
    for(auto &comp : comp_data_list) {
        for(auto &desc : comp.mesh_descriptors) {
            //strip length and texture name are set Component::export_to_file
            desc.strip_list_offset += strip_offset;

            vec_append_buff(file_buffer, &desc, sizeof(mesh_descriptor_t));
        }
        strip_offset += comp.comp_buffer.size();
    }

    //Append mesh data
    for(auto &comp : comp_data_list) {
        vec_append_vec(file_buffer, comp.comp_buffer);
    }

    //Append joint data
    vec_append_vec(file_buffer, joint_data);

    //append dictionary
    for(auto const& s : dictionary) {
        vec_append_str(file_buffer, s);
    }

    //append "end" with no terminator
    vec_append_buff(file_buffer, "end", 3);

    buffer_to_file(file_buffer, file_name);
}

std::string Model::getName() const {
    return name;
}

std::string Model::getFilename() const {
    return name + ".mdl";
}

Animator const& Model::getAnimator() const {
    return anim;
}

void Model::setAnimator(Animator const& anim) {
    this->anim = anim;
}

std::list<Component> const& Model::getComponents() const {
    return this->components;
}

void Model::addComponent(Component const& c) {
    this->components.push_back(c);
}