#pragma once

#include <algorithm>
#include <cassert>
#include <initializer_list>
#include <optional>
#include <unordered_map>
#include <vector>

#include "mk1map.hpp"

using namespace MK1_MAP;
struct edge_t {
    v_index_t v1, v2;

    bool operator==(edge_t& e) {
        return (this->v1 == e.v1) && (this->v2 == e.v2);
    }

    bool operator==(edge_t const& e) const {
        return (this->v1 == e.v1) && (this->v2 == e.v2);
    }

    void order() {
        if (v1 > v2) {
            std::swap(v1, v2);
        }
    }

    edge_t ordered() const {
        edge_t e = *this;
        e.order();
        return e;
    }
};

std::ostream& operator<< (std::ostream &out, edge_t &val) {
    edge_t e = val.ordered();
    out << e.v1 << ", " << e.v2;
    return out;
}

std::ostream& operator<< (std::ostream &out, edge_t const& val) {
    edge_t e = val.ordered();
    out << e.v1 << ", " << e.v2;
    return out;
}

namespace std {
    template <>
    struct hash<edge_t> {
        size_t operator()(const edge_t &e) const {
            std::hash<v_index_t> hash;
            return hash(e.v1) ^ hash(e.v2);
        }
    };
}

typedef s32_t f_index_t;

class strip_generator {

    std::unordered_multimap<edge_t, f_index_t> edge_map;
    std::vector<Face> face_list;
    std::vector<int> adj_count;
    std::vector<bool> face_tracker;

    v_index_t get_opposing_vert(f_index_t f_idx, edge_t const& e) {
        Face f = face_list.at(f_idx);
        std::list<v_index_t> v = {f.v1, f.v2, f.v3};

        v.remove(e.v1);
        v.remove(e.v2);

        assert(v.size() == 1);

        return v.front();
    }

    f_index_t get_next_face(edge_t e, f_index_t nf) {
        auto range = edge_map.equal_range(e);
        int dist = std::distance(range.first, range.second);
        assert(dist == 1 || dist == 2);

        for(auto it = range.first; it != range.second; it++){
            if(it->second != nf) {
                return it->second;
            }
        }
        return -1;
    }

    bool compute_best_strip(f_index_t f, bool force, std::vector<v_index_t> &out) {
        std::list<edge_t> edges;
        for(auto e : edge_map) {
            if(e.second == f) {
                edges.push_back(e.first);
            }
        }

        assert(edges.size() == 3);

        using tristrip_data = std::pair<std::vector<v_index_t>, std::vector<f_index_t>>;

        std::vector<tristrip_data> tristrips;
        for(auto edge : edges) {
            tristrip_data strip_data;
            generate_strip(f, edge, strip_data.first, strip_data.second);
            tristrips.push_back(strip_data);
        }

        tristrip_data best = *std::max_element(tristrips.begin(), tristrips.end(), [](auto &a, auto &b) {
            return a.first.size() < b.first.size();
        });

        //optimization here further described below:
        // on the first iteration of the face list, we try to skip low face-count strips. A good number seems to be 3
        //This does not apply when the force flag is set.
        if(best.first.size() < 5 && !force) {
            return false;
        }
        else {
            for(auto const& f : best.second) {
                assert(!face_tracker[f]);
                face_tracker[f] = true;
            }

            out = best.first;

            return true;
        }
    }

    void generate_strip(f_index_t f, edge_t e, std::vector<v_index_t> &strip, std::vector<f_index_t> &face_usage) {
        f_index_t this_face = f;
        edge_t this_edge = e;

        strip.push_back(e.v1);
        strip.push_back(e.v2);

        while(true) {
            face_usage.push_back(this_face);

            edge_t next_edge;
            next_edge.v1 = strip.back();
            strip.push_back(get_opposing_vert(this_face, this_edge));
            next_edge.v2 = strip.back();

            this_edge = next_edge.ordered();

            this_face = get_next_face(this_edge, this_face);
            if( this_face == -1
             || std::find(face_usage.begin(), face_usage.end(), this_face) != face_usage.end()
             || face_tracker[this_face]) {
                 break;
             }
        }
    }

public:
    strip_generator() = delete;

    strip_generator(std::vector<Face> const& face_list) {
        // halfedge_map_t temp_map;

        this->face_list = std::vector<Face>(face_list);

        //first pass to map edges to faces
        for(auto it = face_list.begin(); it != face_list.end(); it++ ) {
            auto &f = *it;
            f_index_t f_idx = std::distance(face_list.begin(), it);

            edge_t e1 = {f.v1, f.v2};
            edge_t e2 = {f.v2, f.v3};
            edge_t e3 = {f.v3, f.v1};

            edge_map.insert({e1.ordered(), f_idx});
            edge_map.insert({e2.ordered(), f_idx});
            edge_map.insert({e3.ordered(), f_idx});
        }

        adj_count.resize(face_list.size());
        //this will iterate only keys in a multimap
        for(auto it = edge_map.cbegin(); it != edge_map.cend(); it = edge_map.equal_range(it->first).second) {
            auto range = edge_map.equal_range(it->first);
            for(auto it = range.first; it != range.second; it++){
                adj_count[it->second]++;
            }
        }

        std::sort(adj_count.begin(), adj_count.end(), [](auto &a, auto &b) { return a < b; });

        face_tracker.resize(face_list.size());
        face_tracker.assign(face_tracker.size(), false);
    }

    void generate_strips(std::list<std::vector<v_index_t>> &strip_list ) {
        strip_list.clear();

        //check if any faces have not been added
        // the vector<bool> operation makes this very fast
        bool first_iter = true;
        while(std::count(face_tracker.begin(), face_tracker.end(), false) > 0) {
            for(int i = 0; i < adj_count.size(); i++) {
                if(!face_tracker[i]) {
                    std::vector<v_index_t> strip;
                    //small optimization:
                    // we try to avoid single triangle strips on the first iteration by telling
                    // the computation function it's allowed to skip a starting face
                    //After the first loop iteration we force it to return single face strips to
                    // pick up stragglers.
                    //We do need to tell it not to skip faces that don't have any adjacent faces, should they exist
                    if (compute_best_strip(i, !first_iter || adj_count[i] == 0, strip)) {
                        strip_list.push_back(strip);
                    }
                }
            }
            first_iter = false;
        }
    }
};
