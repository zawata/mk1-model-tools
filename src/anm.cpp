#include "mk1map.hpp"

#include "anm.hpp"

#include <cstring>
#include <fstream>
#include <iterator>
#include <iomanip>

#include "section_defs.h"
#include "exceptions.hpp"

#define as_hex(u) std::hex << ((unsigned long long)u) << std::dec

using namespace MK1_MAP;

Frame::Frame(int frame_id) : frame_id(frame_id) {}

std::map<b_index_t, Matrix4f> const& Frame::getTransforms() const {
    return this->transforms;
}

int Frame::getFrameID() const {
    return frame_id;
}

// Vector3f gen_mk1_trans(Quaternionf q, Vector3f s, Vector3f p) {
//     float rt_2 = 1.41421354f; // sqrt(2)

//     float qx = q.x() * rt_2;
//     float qy = q.y() * rt_2;
//     float qz = q.z() * rt_2;
//     float qw = q.w() * rt_2;
//     float sx = s[0];
//     float sy = s[1];
//     float sz = s[2];

//     float px = p[0];
//     float py = p[1];
//     float pz = p[2];

//     sx *= px;
//     sy *= py;
//     sz *= pz;

//     float x2_y2 = (qx * qx) + (qy * qy);
//     float y2_z2 = (qy * qy) + (qz * qz);
//     float x2_z2 = (qx * qx) + (qz * qz);

//     float f1 = (qz * qx) + (qw * qy);
//     float f2 = (qw * qz) + (qy * qx);
//     float f3 = (qw * qx) + (qz * qy);
//     float f4 = (qy * qx) - (qw * qz);
//     float f5 = (qz * qy) - (qw * qx);
//     float f6 = (qx * qz) - (qw * qy);

//     return Vector3f({
//         (((y2_z2 - 1.0f) * sx - f2 * sy) - f6 * sz) + px,
//         (((x2_z2 - 1.0f) * sy - f4 * sx) - f3 * sz) + py,
//         ((x2_y2 - 1.0f) * sz - (f5 * sy + f1 * sx)) + pz
//     });
// }

/**
 * Animator Implementation
 **/
Animator::Animator() {}

Animator *Animator::importFromFile(std::string filename) {
    Animator anim;

    std::vector<u8_t> file_data;

    file_to_buffer(filename, file_data);

    anm_header_t *a = (anm_header_t *)file_data.data();
    anim.bones.reserve(a->bone_desc_count);

    //first iteration to create bone data
    for(u32_t i = 0; i < a->bone_desc_count; i++) {
        off32_t descriptor_offset = a->bone_desc_offset + (i * ANM_BONE_DESCRIPTOR_SZ);
        anm_bone_descriptor_t *b = (anm_bone_descriptor_t *)vec_get_ptr(file_data, descriptor_offset);

        Bone new_bone;
        new_bone.name = std::string((char*)vec_get_ptr(file_data, b->joint_name_offset));
        new_bone.position = Vector3f(b->joint_pos);

        anim.bones.push_back(new_bone);
    }

    //second iteration to create Bone Tree and extract Frame Data
    for(u32_t i = 0; i < a->bone_desc_count; i++) {
        off32_t descriptor_offset = a->bone_desc_offset + (i * ANM_BONE_DESCRIPTOR_SZ);
        anm_bone_descriptor_t *b = (anm_bone_descriptor_t *)vec_get_ptr(file_data, descriptor_offset);

        anim.bones.at(i).parent = b->parent_id;
        if(b->parent_id == -1) {
            anim.bones.at(b->parent_id).children.push_back(i);
        }

        for(u32_t j = 0; j < b->data_count; j++) {
            off32_t bone_transform_offset = b->data_offset + ( j * 0xC );

            u32_t *bone_transform_component_offsets = (u32_t *)vec_get_ptr(file_data, bone_transform_offset);

            float f[4];
            std::memcpy(f, vec_get_ptr(file_data, bone_transform_component_offsets[0]), 0x10);
            //Vector3f position = convert_position(f[0], f[1], f[2]);
            Vector3f position;
            position << f[0], f[1], f[2];
            u32_t frame_id = ((u32_t *)f)[3];

            std::memcpy(f, vec_get_ptr(file_data, bone_transform_component_offsets[1]), 0x10);
            Quaternionf rotation(
                f[3],
                f[0],
                f[1],
                f[2]);

            std::memcpy(f, vec_get_ptr(file_data, bone_transform_component_offsets[2]), 0x10);
            Vector3f scale;
            scale << f[0], f[1], f[2];

            Transformf frame_transform = Transformf::Identity();

            frame_transform
                .scale(scale)
                .rotate(rotation.normalized())
                //.translate(gen_mk1_trans(rotation, scale, bone->position))
                .translate(position);

            Eigen::IOFormat form(4, 0, ",", "", "(", ")", "", "");

            // std::cout << "bone: " << i << ", frame: " << frame_id << std::endl;
            // std::cout << "\t" << "scale: " << scale.format(form) << std::endl;
            // std::cout << "\t" << "rotation: " << rotation.w() << ", "  << rotation.vec().format(form) << std::endl;
            // std::cout << "\t" << "offset " << gen_mk1_trans(rotation, scale, bone->position).format(form) << std::endl;
            // std::cout << "\t" << "position: " << position.format(form) << std::endl;

            Eigen::IOFormat MatFmt(4, 0, ", ", "\n", "\t\t[", "]");
            // std::cout << "\t" << "Transform: " << std::endl << frame_transform.matrix().transpose().format(MatFmt) << std::endl;

            anim.addFrameData(frame_id, i, frame_transform.matrix());
        }
    }

    return new Animator(anim);
}

Animator::~Animator() {}

void Animator::addFrameData(int frame_id, b_index_t b, Matrix4f const& m) {
    //This will create the frame object if it doesn't exist
    auto &frame = this->frames.insert({frame_id,Frame(frame_id)}).first->second;
    frame.transforms.insert({b,m});
}

std::vector<Bone> const& Animator::getBones() const {
    return this->bones;
}

std::map<int, Frame> const& Animator::getFrames() const {
    return this->frames;
}