set(BASE_SRCS
        anm.cpp
        mdl.cpp
        mk1map.cpp
        )

# Static Library Code
add_library(s_mk1map STATIC
        ${BASE_SRCS})
set_target_properties(s_mk1map PROPERTIES
        POSITION_INDEPENDENT_CODE ON)

add_subdirectory("./impl")
add_subdirectory("./mains")