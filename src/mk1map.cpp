#include <fstream>
#include <iterator>
#include <sstream>
#include <unordered_map>

#include "exceptions.hpp"
#include "mk1map.hpp"

std::unordered_map<std::string, std::string> alias_map;

const char * MK1_MAP::get_ver_str() {
    return VERSION_STR;
}

int MK1_MAP::get_ver() {
    return VERSION;
}

void MK1_MAP::load_alias_list(std::string file) {
    std::ifstream f_strm;
    f_strm.open(file, std::fstream::in);
    if(!f_strm) {
        throw FileNotFoundException(file);
    }

    if(alias_map.size() > 0) {
        alias_map.clear();
    }

    std::string line;
    while (std::getline(f_strm, line)) {
        std::istringstream iss(line);
        std::string alias, actual;
        if (!(iss >> alias >> actual)) { break; } // error

        //convert everything to lowercase
        std::transform(alias.begin(), alias.end(), alias.begin(),
            [](unsigned char c){ return std::tolower(c); });
        std::transform(actual.begin(), actual.end(), actual.begin(),
            [](unsigned char c){ return std::tolower(c); });

        alias_map[alias] = actual;
    }
}

void file_to_buffer(std::string filename, std::vector<u8_t> &filebuffer) {
    std::ifstream f_strm;
    f_strm.open(filename, std::fstream::in | std::fstream::binary);
    if(!f_strm) {
        throw FileNotFoundException(filename);
    }

    f_strm.seekg(std::ios::end);
    size_t file_sz = f_strm.tellg();
    f_strm.seekg(std::ios::beg);

    filebuffer.reserve(file_sz);
    filebuffer.assign(std::istreambuf_iterator<char>(f_strm), std::istreambuf_iterator<char>());

    f_strm.close();
}

void buffer_to_file(std::vector<u8_t> const& filebuffer, std::string filename) {
    std::ofstream f_strm;
    f_strm.open(filename, std::fstream::out | std::fstream::binary);
    if(!f_strm) {
        assert(false); //TODO: what circumstances can this fail under?
    }

    std::copy(filebuffer.begin(), filebuffer.end(), std::ostream_iterator<u8_t>(f_strm));

    f_strm.close();
}
