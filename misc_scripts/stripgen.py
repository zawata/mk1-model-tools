
# 1   2   3   4   5   6   7   8
# +---+---+---+---+---+---+---+
# |  /|  /|  /|  /|  /|  /|  /|
# | / | / | / | / | / | / | / |
# |/  |/  |/  |/  |/  |/  |/  |
# +---+---+---+---+---+---+---+
# 9   10  11  12  13  14  15  16
#
# best strip:
# 1,9,2
# 9,2,10
# 2,10,3
# 10,3,11
# 3,11,4
# 11,4,12
# 4,12,5
# 12,5,13
# 5,13,6
# 13,6,14
# 6,14,7
# 14,7,15
# 7,15,8
# 15,8,16
#
# 1,9,2,10,3,11,4,12,5,13,6,14,7,15,8,16

# face_list = [
#     (1,2,9),
#     (2,3,10),
#     (3,4,11),
#     (4,5,12),
#     (5,6,13),
#     (6,7,14),
#     (7,8,15),
#     (9,10,2),
#     (10,11,3),
#     (11,12,4),
#     (12,13,5),
#     (13,14,6),
#     (14,15,7),
#     (15,16,8)
# ]

face_list = [
    (0, 1, 2),
    (2, 1, 3),
    (3, 4, 2),
    (5, 4, 3),
    (6, 4, 5),
    (5, 7, 6),
    (6, 7, 8),
    (8, 7, 9),
    (9, 10, 8),
    (11, 10, 9),
    (12, 10, 11),
    (11, 13, 12),
    (12, 13, 14),
    (14, 13, 15),
    (12, 14, 16),
    (16, 14, 17),
    (17, 18, 16),
    (19, 18, 17),
    (0, 18, 19),
    (19, 20, 0),
    (0, 20, 1),
    (14, 15, 17),
    (17, 15, 21),
    (21, 19, 17),
    (20, 19, 21),
    (22, 0, 2),
    (18, 0, 22),
    (22, 23, 18),
    (18, 23, 16),
    (16, 23, 24),
    (24, 12, 16),
    (10, 12, 24),
    (25, 23, 22),
    (24, 23, 25),
    (25, 8, 24),
    (24, 8, 10),
    (4, 22, 2),
    (25, 22, 4),
    (4, 6, 25),
    (25, 6, 8),
    (26, 27, 28),
    (28, 27, 29),
    (29, 30, 28),
    (31, 30, 29),
    (32, 30, 31),
    (31, 33, 32),
    (32, 33, 34),
    (34, 33, 35),
    (35, 36, 34),
    (37, 36, 35),
    (38, 36, 37),
    (37, 39, 38),
    (38, 39, 40),
    (40, 39, 41),
    (40, 41, 42),
    (42, 41, 43),
    (43, 44, 42),
    (45, 44, 43),
    (26, 44, 45),
    (45, 27, 26)]

face_list2 = {
    {0, 1, 2},
    {3, 4, 5},
    {6, 7, 8},
    {9, 10, 11},
    {12, 13, 14},
    {15, 16, 17},
    {18, 19, 20},
    {21, 22, 23},
    {24, 25, 26},
    {27, 28, 29},
    {30, 31, 32},
    {33, 34, 35},
    {36, 37, 38},
    {39, 40, 41},
    {42, 43, 44},
    {45, 46, 47},
    {48, 49, 50},
    {51, 52, 53},
    {54, 55, 56},
    {57, 58, 59},
    {60, 61, 62},
    {63, 64, 65},
    {66, 67, 68},
    {69, 70, 71},
    {72, 73, 74},
    {75, 76, 77},
    {78, 79, 80},
    {81, 82, 83},
    {84, 85, 86},
    {87, 88, 89},
    {90, 91, 92},
    {93, 94, 95},
    {96, 97, 98},
    {99, 100, 101},
    {102, 103, 104},
    {105, 106, 107},
    {108, 109, 110},
    {111, 112, 113},
    {114, 115, 116},
    {117, 118, 119},
    {120, 121, 122},
    {123, 124, 125},
    {126, 127, 128},
    {129, 130, 131},
    {132, 133, 134},
    {135, 136, 137},
    {138, 139, 140},
    {13, 39, 141},
    {142, 143, 144},
    {145, 146, 147},
    {40, 66, 148},
    {149, 150, 151},
    {152, 153, 154},
    {67, 93, 155},
    {156, 157, 158},
    {159, 160, 161},
    {94, 120, 162},
    {163, 164, 165},
    {166, 167, 168},
    {121, 12, 169},
    {170, 171, 172},
    {173, 174, 175},
    {176, 177, 178},
    {179, 180, 181},
    {182, 183, 184},
    {185, 186, 187},
    {188, 189, 190},
    {191, 192, 193},
    {194, 195, 196},
    {197, 198, 199},
    {200, 201, 202},
    {203, 204, 205},
    {206, 207, 208},
    {209, 210, 211},
    {212, 213, 214},
    {215, 216, 217},
    {218, 219, 220},
    {221, 222, 223},
    {224, 225, 226},
    {227, 228, 229},
    {230, 231, 232},
    {233, 234, 235},
    {236, 237, 238},
    {239, 240, 241},
    {242, 243, 244},
    {245, 246, 247},
    {248, 249, 250},
    {251, 252, 253},
    {254, 255, 256},
    {257, 258, 259},
    {260, 261, 262},
    {263, 264, 265},
    {266, 267, 268},
    {269, 270, 271},
    {272, 273, 274},
    {275, 276, 277},
    {278, 279, 280},
    {281, 282, 283},
    {284, 285, 286},
    {287, 288, 289},
    {290, 291, 292},
    {293, 294, 295},
    {296, 297, 298},
    {299, 300, 301},
    {302, 303, 304},
    {305, 306, 307},
    {308, 309, 310},
    {311, 312, 313},
    {314, 315, 316},
    {317, 318, 319},
    {320, 321, 322},
    {323, 324, 325},
    {326, 327, 328},
    {329, 330, 331},
    {332, 333, 334},
    {335, 336, 337},
    {338, 339, 340},
    {341, 342, 343},
    {344, 345, 346},
    {347, 348, 349}}

class StripGenerater():
    # [ (<v_idxs of face>), (<v_idxs of face>), (<v_idxs of face>)]
    face_list = None

    # {
    #   <'sorted_edge'>: <list of faces using said edge>
    # }
    edge_dict = None

    # [
    #   [<face_index>,<list of adj faces>],
    #   ...
    # ]
    conn_list = None

    # [<face_used?>, <face_used?>, <face_used?>]
    face_usage = None

    @staticmethod
    def _sort_edge(e):
        assert(len(e) == 2)
        return (min(e), max(e))

    @staticmethod
    def _add_or_append(dct, key, val):
        if key in dct:
            dct[key].append(val)
        else:
            dct[key] = [val]

    @staticmethod
    def _get_third_vert(face, edge):
        f_list = list(face)
        f_list.remove(edge[0])
        f_list.remove(edge[1])

        assert(len(f_list) == 1)
        return f_list[0]

    def __init__(self, faces):
        self.face_list = faces

        edge_dict = {}
        for i,f in enumerate(faces):
            assert(len(f) == 3)

            self._add_or_append(edge_dict, self._sort_edge(f[:2]), i)
            self._add_or_append(edge_dict, self._sort_edge(f[0::2]), i)
            self._add_or_append(edge_dict, self._sort_edge(f[1:]), i)

        self.edge_dict = edge_dict

        adj_list = [[i,0] for i in range(len(faces))]
        for _,face_list in edge_dict.items():
            if len(face_list) > 1:
                adj_list[face_list[0]][1] += 1
                adj_list[face_list[1]][1] += 1
        adj_list.sort(key=lambda x:x[1])

        self.conn_list = adj_list

        self.face_usage = [False] * len(faces)

    def get_edges_of_face(self, face):
        edges = []
        for e,f_list in self.edge_dict.items():
            if face in f_list:
                edges.append(e)

        assert(len(edges) == 3)
        return edges

    def mark_face_as_done(self, face):
        assert(not self.face_usage[face])
        self.face_usage[face] = True

    def get_next_start_face(self):
        for i,_ in self.conn_list:
            if not self.face_usage[i]:
                return i
        return None


    def get_next_face(self, edge, not_face):
        f_list = self.edge_dict[self._sort_edge(edge)]
        assert(len(f_list) in [1,2])

        for f in f_list:
            if f != not_face:
                return f
        return None

    def compute_best_strip(self, face):
        #iterate all 3 sides of the triangle
        tristrip = []
        for i,e in enumerate(self.get_edges_of_face(face)):
            tristrip.append(self.gen_strip(face, e))

        best_len = max([len(l) for l,_ in tristrip])
        for l,f_list in tristrip:
            if len(l) == best_len:
                for f in f_list:
                    self.mark_face_as_done(f)
                return l

    def gen_strip(self, face, edge):
        this_face = face
        this_edge = edge
        strip_faces = []
        tristrip = []

        tristrip.append(this_edge[0])
        tristrip.append(this_edge[1])
        while True:
            tristrip.append(self._get_third_vert(self.face_list[this_face], this_edge))
            strip_faces.append(this_face)

            this_edge = self._sort_edge(tristrip[-2:])
            this_face = self.get_next_face(this_edge, this_face)
            if not this_face or this_face in strip_faces or self.face_usage[this_face]:
                break
        #TODO: reverse generation
        return (tristrip, strip_faces)

    def gen_strips(self):
        strip_list = []

        while False in self.face_usage:
            next_face = self.get_next_start_face()
            strip_list.append(self.compute_best_strip(next_face))
        return strip_list

if __name__ == '__main__':
    sg = StripGenerater(face_list2)

    print("Strips:")
    for s in sg.gen_strips():
        print(s)