import console_python

console_python.get_console.consoles.pop(hash([r for r in [a for a in bpy.context.screen.areas if a.type == 'CONSOLE'][0].regions if r.type == 'WINDOW'][0]))

import sys
from mathutils import Matrix
sys.path.append('D:\\ty_Stuff\\mdl_proc\\mdl_mk1\\io-scene-mdl')
import mk1map
mk1map.get_ver()
fil_str = "D:\\ty_Stuff\\output\\Act_04_Bilby"
mdl = mk1map.Model(fil_str + '.mdl', fil_str + '.anm')
anm = mdl.getAnimator()

arm_obj = bpy.data.objects[mdl.getName()]
bpy.context.view_layer.objects.active = arm_obj
bpy.ops.object.mode_set(mode='POSE', toggle=False)

pose_bones = arm_obj.pose.bones

bones = anm.getBones()

frames = anm.getFrames()

for f_idx in frames.keys():
    frame = frames[f_idx]
    # print(f"{f_idx}:", [x[0].name for x in frame.getTransforms()])
    for b,t in frames[f_idx].getTransforms():
        # m.decompose()[1].
        pose_bones[b.name].rotation_mode = 'XYZ'
        pose_bones[b.name].matrix_basis = Matrix(t)
        x,y,z = pose_bones[b.name].rotation_euler
        pose_bones[b.name].rotation_euler = (x,z,y)
        _ = pose_bones[b.name].keyframe_insert("location", frame=f_idx)
        _ = pose_bones[b.name].keyframe_insert("rotation_euler", frame=f_idx)
        _ = pose_bones[b.name].keyframe_insert("scale", frame=f_idx)

for b,t in frames[100].getTransforms():
    p = pose_bones[b.name]
    print(b.id, b.name)
    print(p.matrix_basis)
    print(p.matrix)
    print(p.matrix_channel)

pose_bones['z_Bicep_R'].matrix_basis[0] *= -1
pose_bones['z_Bicep_R'].matrix_basis[2] *= -1

pose_bones['z_ForeArm_R'].matrix_basis[0] *= -1
pose_bones['z_ForeArm_R'].matrix_basis[2] *= -1

pose_bones['z_Hand_R'].matrix_basis[0] *= -1
pose_bones['z_Hand_R'].matrix_basis[2] *= -1

def build_frame_dict(bone_id, frame_list):
    frame_dict = {}
    for f in frame_list:
        for b,t in f.getTransforms():
            if b.id == bone_id:
                frame_dict[f.getFrameID()] = Matrix(t).transposed()
    return frame_dict

def build_frame_roll(frame_dict):
    frame_ids = list(frame_dict.keys())
    frame_roll = []
    prev_idx = 0
    next_idx = 1
    for curr_idx in range(frame_ids[-1]):
        if curr_idx == frame_ids[next_idx]:
            prev_idx += 1
            next_idx += 1
            frame_roll.append(frame_dict[curr_idx])
        else:
            factor = (curr_idx - frame_ids[prev_idx]) / (frame_ids[next_idx] - frame_ids[prev_idx])
            #todo: if this doesn't look right: decompose, then interpolate
            frame_roll.append(frame_dict[frame_ids[prev_idx]].lerp(frame_dict[frame_ids[next_idx]], factor))
    return frame_roll


bone_rolls = [None] * len(bones)
for b in bones:
    bone_rolls[b.id] = build_frame_roll(build_frame_dict(b.id, [f[1] for f in frames.items()]))

frame_ids = list(frames.keys())
def pose_bone(b, f_i, m):
    b_m = bone_rolls[b.id][f_i] @ m
    pose_bones[b.name].matrix = b_m
    if f_i in frame_ids:
        pose_bones[b.name].keyframe_insert("location", frame=f_i)
        pose_bones[b.name].keyframe_insert("rotation_quaternion", frame=f_i)
        pose_bones[b.name].keyframe_insert("scale", frame=f_i)
    if b.children:
        for b_c in b.children:
            pose_bone(b_c, f_i, m)

for f_i in range(frame_ids[-1]):
    pose_bone(bones[10], f_i, Matrix.Identity(4))