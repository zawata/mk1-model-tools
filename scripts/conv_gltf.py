import argparse
import io
import struct
import sys

from pygltflib import *

from wand.image import Image as WandImage

# The module is assumed to be in the same directory as the script
import pymk1map as mk1map

COMPONENT_TYPE_SIZE = {
    BYTE:           1,
    UNSIGNED_BYTE:  1,
    SHORT:          2,
    UNSIGNED_SHORT: 2,
    UNSIGNED_INT:   4,
    FLOAT:          4,
}

COMPONENT_TYPE_ID = {
    BYTE:           'b',
    UNSIGNED_BYTE:  'B',
    SHORT:          'h',
    UNSIGNED_SHORT: 'H',
    UNSIGNED_INT:   'I',
    FLOAT:          'f',
}

COMPONENT_COUNT = {
    SCALAR: 1,
    VEC2: 2,
    VEC3: 3,
    VEC4: 4,
    MAT2: 4,
    MAT3: 9,
    MAT4: 16,
}

component_to_fmt_str = lambda component, type: str(COMPONENT_COUNT[component]) + COMPONENT_TYPE_ID[type]
component_size = lambda component, type: COMPONENT_COUNT[component] * COMPONENT_TYPE_SIZE[type]

#
# Exceptions
#
class ExportError(Exception):
    """Base class for exceptions in this module."""
    def __init__(self, message):
        self.message = message

# NOTE: these functions are wildly inefficient requiring at least 6 maybe even 12 passes to find all the values
# it could be done in a single pass when i care more
def get_min_for_pos(vert_arr):
    return (
        min([v.position[0] for v in vert_arr]).item(),
        min([v.position[1] for v in vert_arr]).item(),
        min([v.position[2] for v in vert_arr]).item(),
        )

def get_max_for_pos(vert_arr):
    return (
        max([v.position[0] for v in vert_arr]).item(),
        max([v.position[1] for v in vert_arr]).item(),
        max([v.position[2] for v in vert_arr]).item(),
        )

def create_index_buffer(idxs):
    bfarr = bytearray(len(idxs) * 12) #3 longs per tri * 4 bytes per long

    #if you value your sanity then don't stare at this line too closely
    # it sets up a format string of faces * 3 Long values,
    # then flattens the list of all faces into a single list of indices and passes it to the function as a "splat"(a tuple internally i think?)
    # this is pretty slow, it'd probably be faster to use ctypes and array.array somehow but I don't feel like figuring it out right now
    struct.pack_into(f"<{len(idxs) * 3}L", bfarr, 0, *[v for f in idxs for v in tuple(f)])
    return bfarr

def create_attr_buffer(vert_arr):
    bfarr = bytearray()
    for v in vert_arr:
        bfarr += bytearray(struct.pack(
            "<3f""3f""4f""2f",
            *v.position,    # position
            *v.normal,      # normal
            *v.color,       # color
            *v.uv_coord     # uv coord
            # v.bone_weight # weights(TODO)
            ))

    return bfarr

def buff_to_uri(buff):
    return f"data:application/octet-stream;base64,{base64.b64encode(buff).decode('utf8')}"

def get_image_buff(image_file, tex_directory):
    with WandImage(filename=str(Path(tex_directory) / image_file)) as img:
        return bytearray(img.make_blob(format='png'))

###
# Importer
###
def mdl_to_gltf(mdl, gltf, tex_directory='dds\\'):
    mat_dict = {}

    scene = Scene()
    for component in mdl.getComponents():
        cnode = Node()
        for mdl_mesh in component.getMeshes():
            mesh = Mesh()

            #NOTE: if a new attribute object is not specified here, it uses the old one because the library statically initializes the `attributes` member
            mesh.primitives.append(Primitive(mode=TRIANGLES, attributes=Attributes()))

            vert_count = len(mdl_mesh.getVertices())
            face_count = len(mdl_mesh.getFaces())

            total_buff = bytearray()

            ##################################
            # DEVELOPERS NOTE                #
            ##################################
            # PAY ATTENTION TO THIS NOTICE   #
            ##################################
            # The below creation steps are in a very particular order
            # gltf prefers to keep lists of objects and use indices into these lists for inter-list references
            #
            # ex. Mesh 0 declares Accessor 1 for positions which uses buffer view 4 which is a view for buffer 5
            #
            # The easiest way to process this is make it so each time an object is created we just grab the length of the array it's about to be appended to.
            # This requires the objects be appended to their respective buffers in a prticular order
            #
            # Therefore all of the following operations are in such an order that they grab the size of arrays. if placed out of order, their indicies may not match up
            # you have been warned.

            #
            # Texture Stack
            #
            # NOTE: modern texture stacks are complex but this model format isn't, so these are mostly default values.
            #

            tex_name = mdl_mesh.getTextureName().lower()

            if tex_name in mat_dict.keys():
                #reuse existing texture
                mesh.primitives[0].material = mat_dict[tex_name]
            else:
                #make a new texture
                mat_dict[tex_name] = len(gltf.materials)
                mesh.primitives[0].material = len(gltf.materials)
                gltf.materials.append(Material(
                    pbrMetallicRoughness=PbrMetallicRoughness(
                        baseColorTexture=TextureInfo(
                            index=len(gltf.textures)
                        ),
                        metallicFactor=0.0,
                        roughnessFactor=1.0
                    ),
                    doubleSided=True #important, mk1 models don't use their defined normals and the game's strip unwinding code screws them up anyways. disable backface-culling otherwise the model comes out checkered
                ))
                gltf.textures.append(Texture(
                    sampler=len(gltf.samplers),
                    source=len(gltf.images)
                ))
                gltf.samplers.append(Sampler(
                    magFilter=LINEAR,
                    minFilter=LINEAR_MIPMAP_LINEAR,
                    wrapS=REPEAT,
                    wrapT=REPEAT
                ))

                gltf.images.append(Image(
                    bufferView=len(gltf.bufferViews),
                    mimeType="image/png",
                    name=mdl_mesh.getTextureName().lower()))

                image_buff = get_image_buff(mdl_mesh.getTextureName() + '.dds', tex_directory)
                gltf.bufferViews.append(BufferView(
                    buffer=len(gltf.buffers),
                    byteOffset=len(total_buff),
                    byteLength=len(image_buff)))
                total_buff += image_buff

                #align the end of the buffer to a 4-byte boundary
                if len(total_buff) % 4:
                    total_buff += bytearray(b'\x00' * (4 - len(total_buff) % 4))

            #
            # Vertex Element Array
            #

            # Vertex Index Accessor
            mesh.primitives[0].indices = len(gltf.accessors)
            gltf.accessors.append(Accessor(
                    bufferView=len(gltf.bufferViews),
                    byteOffset=0,
                    componentType=UNSIGNED_INT,
                    count=face_count * 3,
                    type=SCALAR))

            # Index Array View
            idx_verts = create_index_buffer(mdl_mesh.getFaces())
            gltf.bufferViews.append(BufferView(
                    buffer=len(gltf.buffers),
                    byteOffset=len(total_buff),
                    byteLength=len(idx_verts)))
            total_buff += idx_verts

            #
            # Vertex Attribute Array
            #

            # Position Attribute Accessor
            mesh.primitives[0].attributes.POSITION = len(gltf.accessors)
            gltf.accessors.append(Accessor(
                    bufferView=len(gltf.bufferViews),
                    byteOffset=0,
                    componentType=FLOAT,
                    count=vert_count,
                    type=VEC3,
                    min=get_min_for_pos(mdl_mesh.getVertices()),
                    max=get_max_for_pos(mdl_mesh.getVertices()),))

            # Normal Attribute Accessor
            mesh.primitives[0].attributes.NORMAL = len(gltf.accessors)
            gltf.accessors.append(Accessor(
                    bufferView=len(gltf.bufferViews),
                    byteOffset=12, # sizeof(float) * 3
                    componentType=FLOAT,
                    count=vert_count,
                    type=VEC3))

            # Color Attribute Accessor
            mesh.primitives[0].attributes.COLOR_0 = len(gltf.accessors)
            gltf.accessors.append(Accessor(
                    bufferView=len(gltf.bufferViews),
                    byteOffset=24, # sizeof(float) * 6
                    componentType=FLOAT,
                    count=vert_count,
                    type=VEC4))

            # UV Attribute Accessor
            mesh.primitives[0].attributes.TEXCOORD_0 = len(gltf.accessors)
            gltf.accessors.append(Accessor(
                    bufferView=len(gltf.bufferViews),
                    byteOffset=40, # sizeof(float) * 10
                    componentType=FLOAT,
                    count=vert_count,
                    type=VEC2))

            # Attribute Array View
            vert_attr = create_attr_buffer(mdl_mesh.getVertices())
            gltf.bufferViews.append(BufferView(
                    buffer=len(gltf.buffers),
                    byteStride=48, # 12 floats
                    byteOffset=len(total_buff),
                    byteLength=len(vert_attr)))
            total_buff += vert_attr

            #data buffer
            gltf.buffers.append(Buffer(
                    uri=buff_to_uri(total_buff),
                    byteLength=len(total_buff)))

            #create the current mesh's node
            mnode = Node()
            mnode.mesh = len(gltf.meshes)

            #append the mesh created above
            gltf.meshes.append(mesh)

            # add the mesh's node as a child of the component node
            cnode.children.append(len(gltf.nodes))
            #add the mesh's node to the file
            gltf.nodes.append(mnode)
        #set the component node's id to appear in the scene
        scene.nodes.append(len(gltf.nodes))
        #add the component's node ot the file
        gltf.nodes.append(cnode)
    #append the scene to the file
    gltf.scenes.append(scene)

###
# Exporter
###
def gltf_to_mdl(gltf: GLTF2, mdl: mk1map.Model, tex_directory='dds\\'):
    #take only the default scene
    scene = None
    if gltf.scene:
        scene = gltf.scenes[gltf.scene]
    else:
        scene = gltf.scenes[0]
    if not scene:
        raise ExportError("No scenes are defined")

    buffers = []
    for i in range(len(gltf.buffers)):
        #TODO: non-uri buffers

        buffer = str(gltf.buffers[i].uri)
        assert(buffer[:37] == "data:application/octet-stream;base64,")

        buffers.append(base64.b64decode(buffer[37:]))

    mat_id_to_image_str = []
    for material in gltf.materials:
        #sanity check that we only have pbr materials and they have base color textures
        assert(material.pbrMetallicRoughness != None)
        assert(material.pbrMetallicRoughness.baseColorTexture != None)

        image_id = gltf.textures[material.pbrMetallicRoughness.baseColorTexture.index].source

        if gltf.images[image_id].name.count('.'):
            pass #TODO: images are in local directory but they still need to be converted
        else:
            print(f"image {gltf.images[image_id].name} appears to be in a buffer, decoding to the current directory")

            assert(gltf.images[image_id].bufferView != None)
            assert(gltf.images[image_id].mimeType == "image/png")

            bfv = gltf.bufferViews[gltf.images[image_id].bufferView]
            img_buff = buffers[bfv.buffer][bfv.byteOffset:bfv.byteOffset + bfv.byteLength]

            with WandImage(blob=img_buff, format='png') as wnd_img:
                with wnd_img.convert('dds') as cnvt_img:
                    cnvt_img.save(filename=Path(tex_directory) / (gltf.images[image_id].name + '.dds'))

            #decode the material ids to image ids
            mat_id_to_image_str.append(gltf.images[image_id].name)

    meshes = {}
    components = []
    for i in range(len(gltf.nodes)):
        node = gltf.nodes[i]
        if node.mesh != None:
            meshes[i] = gltf.meshes[node.mesh]

        elif node.children != None:
            components.append(node)

    def get_buff_slice(bfv):
        return buffers[bfv.buffer][bfv.byteOffset:bfv.byteOffset + bfv.byteLength]

    def parse_indices(acc_idx):
        acc = gltf.accessors[acc_idx]
        buff = get_buff_slice(gltf.bufferViews[acc.bufferView])

        return [i[0] for i in struct.iter_unpack('<'+component_to_fmt_str(SCALAR, acc.componentType), buff)]

    def parse_single_attr(acc : Accessor):
        bfv = gltf.bufferViews[acc.bufferView]
        buff_slice = get_buff_slice(bfv)

        if bfv.byteStride != None:
            #mesh uses multiple accessors for 1 bufferview
            #ie. vert attributes are interleaved(this is how we do it)

            pad_size = bfv.byteStride - (component_size(acc.type, acc.componentType) + acc.byteOffset)

            fmt_str = ('x' * acc.byteOffset) + component_to_fmt_str(acc.type, acc.componentType) + ('x' * pad_size)
            return struct.iter_unpack(fmt_str, buff_slice)

        else:
            #mesh uses 1 accessor per bufferview
            #ie. vert attributes are concatenated(this is how blender does it)

            return struct.iter_unpack(component_to_fmt_str(acc.type, acc.componentType), buff_slice)

            pass

    def parse_attrs(attr_obj : Attributes):
        assert(attr_obj.POSITION != None)
        assert(attr_obj.NORMAL != None)
        assert(attr_obj.TEXCOORD_0 != None)
        assert(attr_obj.COLOR_0 != None)
        acc_s = {
            POSITION:   gltf.accessors[attr_obj.POSITION],
            NORMAL:     gltf.accessors[attr_obj.NORMAL],
            TEXCOORD_0: gltf.accessors[attr_obj.TEXCOORD_0],
            COLOR_0:    gltf.accessors[attr_obj.COLOR_0]
        }

        iter_list = (
            parse_single_attr(acc_s[POSITION]),
            parse_single_attr(acc_s[NORMAL]),
            parse_single_attr(acc_s[TEXCOORD_0]),
            parse_single_attr(acc_s[COLOR_0]))

        vert_list = []
        for (i,(pos,norm,tex_0,col_0)) in enumerate(zip(*iter_list)):
            vert = mk1map.Vertex()
            vert.position = pos
            vert.normal = norm
            vert.color = col_0
            vert.uv_coord = tex_0
            vert_list.append(vert)
        return vert_list

    #TODO: what about a single node, no sub nodes.
    for node in components:
        mdl_meshes = []
        for child in node.children:
            if child in meshes:
                gltf_mesh = meshes[child]

                #TODO: if this becomes an issue, we can make each primitive it's own mesh using the same texture
                assert(len(gltf_mesh.primitives) == 1)

                if gltf_mesh.primitives[0].mode not in [None, TRIANGLES]:
                    assert(False)

                mdl_mesh = mk1map.Mesh(mat_id_to_image_str[gltf_mesh.primitives[0].material])

                vert_list = parse_attrs(gltf_mesh.primitives[0].attributes)
                mdl_mesh.addVertices(vert_list)

                #python trick here to convert a flat list into a list of tuples
                # create an iterator and pass it to the zip function multiple times
                # https://stackoverflow.com/questions/23286254
                idx_list = parse_indices(gltf_mesh.primitives[0].indices)
                tri_list = zip(*[iter(idx_list)]*3)

                for tri in tri_list:
                    mdl_mesh.addFace(mk1map.Face(tri))

                mdl_meshes.append(mdl_mesh)
        if meshes:
            mdl_comp = mk1map.Component()
            mdl_comp.addMeshes(mdl_meshes)
            mdl.addComponent(mdl_comp)

if __name__ == "__main__":
    print(f"MK1MAP version: {mk1map.get_ver()}")

    parser = argparse.ArgumentParser(description='Convert between MDL and GLTF 3d model formats.')
    parser.add_argument('input', help='input file')
    parser.add_argument('output', help='output file')
    parser.add_argument('-f', '--output-format', help='format of the output file', choices=['mdl', 'gltf'])
    parser.add_argument('--alias-list', help='path to alias list. Defaults to \'./alias_list.txt\'', default='./alias_list.txt')
    parser.add_argument('--texture-directory', help='directory to lookup textures from. Defaults to \'./dds\'', default='./alias_list.txt')

    args = parser.parse_args(sys.argv[1:])

    print(f"Loading alias_list.txt from {args.alias_list}")
    mk1map.load_alias_list(args.alias_list)

    if args.output_format == 'mdl':
        input_gltf = None
        if(args.input.endswith('.gltf')):
            input_gltf = GLTF2.load(args.input)
        else:
            print("Input file must be .gltf")
            exit(-1)
        mdl = mk1map.Model("fil_name")
        gltf_to_mdl(input_gltf, mdl, args.texture_directory)
        mdl.exportToFile(args.output)
    elif args.output_format == 'gltf':
        input_mdl = None
        if(args.input.endswith('.mdl')):
            input_mdl = mk1map.Model.importFromFile(args.input)
        else:
            print("Input file must be .mdl")
            exit(-1)
        gltf = GLTF2()
        mdl_to_gltf(input_mdl, gltf, args.texture_directory)
        gltf.save(args.output)
    print("File converted. Exiting")
