# Background:
Model Data are originally adapted from `.3ds` files produced by 3dsmax.\

Internally this format is referred to as MDL2.

The Merkury MDL2 file format was only utilized in Merkury V1. V2 utilized a new format involving a 2 files: `.mdl` and `.mdg`.

The `.mdl` file extension(being a shorthand for "Model") is utilized in several other engines:
 * Quake I
 * [3D Game Studio](http://www.3dgamestudio.com/)
 * Halflife 1 & 2

None of these formats are close to the MDL2 format. Information about those formats can be found elsewhere pretty easily

# Model Data Format

An MDL2 files is comprised of 5 main parts:
 * File Header
 * Component Data
 * Section 2
 * Section 3
 * Ditionary

0x70 byte Header:

|Offset|Length|Type|Function|Notes|
|-|-|-|-|-|
|0x00|4|char[4]|File ID|must be MDL2|
|0x04|2|ushort|||
|0x06|2|ushort|Component Count||
|0x08|2|ushort|Section 2 Block Count||
|0x0A|2|ushort|Section 3 Block Count||
|0x0C|4|uint|Component Data Offset|Usually 0x70|
|0x10|4|uint|Section 2 Block Offset||
|0x14|4|uint|Section 3 Block Offset||
|0x18|4|uint||The code seems to indicate this is an offset?|
|0x1C|4|uint|||
|0x20|4|vector4f|||
|0x30|4|vector4f|||
|0x40|4|uint|||
|0x44|4|uint|Dictionary Offset||
|0x48|4|uint|||
|0x50|4|unix time|Creation Time||
|0x54|4|uint|Original File Offset||
|0x58|4|uint|||
|0x5C|2|ushort|||
|0x5E|2|ushort|||
|0x60|4|uint|||
|0x64|4|uint|||
|0x68|4|uint|||
|0x6C|4|uint|||

## Component Data

Starting at the Component Offset is a list of concatenated Component Descriptors

0x50 byte Component Descriptor:

|Offset|Length|Type|Function|Notes|
|-|-|-|-|-|
|0x00|4|vector4f|||
|0x10|4|vector4f|||
|0x20|4|vector4f|||
|0x30|4|uint|Component Name Offset||
|0x34|4|uint||Another String Offset, usually points to a null in the dictionary|
|0x38|4|float|||
|0x3C|4|uint||Gets checked multiple times in the preprocessor|
|0x40|4|uint|||
|0x42|4|ushort|Mesh Descriptor Count||
|0x44|4|uint|Mesh Descriptor Offset||
|0x48|4|uint|||
|0x50|4|||Set to malloc'd area depending on 0x3C|

Starting at the Component Offset is a list of concatenated Mesh Descriptors

0x10 byte Mesh Descriptor:

|Offset|Length|Type|Function|Notes|
|-|-|-|-|-|
|0x00|4|uint|Texture Name Offset|Gets replaced by a pointer to the file instance|
|0x04|4|uint|Mesh Data Offset||
|0x08|4|uint||not a reasonable number or float and wildly different per entry. CRC?|
|0x0C|4|uint|Mesh Data Entry Count||


Meshes are comprised of concatenated Vertex Lists arranged in unrolled Triangle Strips.

Each Vertex List starts with a short header. The only part of this header derived thus far(and the only data reference from the header) is the `uint` vertex count value at `0xC` from the start of the Entry.

Vertex Lists encode their vertices by splitting them into 5 parts and combining these parts into lists with other vertices. Each mesh data entry contains 5 parts: header, float data, char data, short data, and vertex colors.

Interesting note: the start of each section appears to start with a tag of the form `0X 80 YY 6Z` where `X` is the section number + 1, `YY` is the vertex count for the mesh, and `Z` may denote the section type?

The float data section starts at Entry Offset `0x30`. +4 for the start tag. This section contains a list of `vector3f` for representing vertex positions
vertex

The char data section starts at the end of the float data section. +4 for the start tag. This section contains a list of `char[4]` with the first `char[3]` being `uchar` representations of the vertex normal components multiplied by 2. The last is used as 1 of 2 bone indices, to be described later.

The short data section starts at the end of the char data section. +4 for the start tag. This section contains a list of `short[4]`. The first 2 are uv coordinates on a 10bit scale. The latter are used as bone data, to be covered later.

The Vertex color data section starts at the end of the short data section. +4 for the start tag. data is the next sectioon and is a list of `char[4]` representing RGBA data. Each byte is encoded such that values less than 127 are odd numbers and values greater, even. in general the following function is used to convert this data to a normal unsigned char scale: `(c < 0x80) ? (2*c)-1 : (c-1)*2`

Bones Indicies and Weights are also encoded into the mesh but do not comprise their own section, instead taking an unused value from some of the previous sections. Each vertex maybe be affected by up to 2 Bones but uses one weight value. The first Bones Index is the 4th short value from the float_data section. The Second Bone Index is the 4th char value from the char data section. Both must be left shifted by 2 and 1 respectively, and are 1-indexed values. The weight value is encoded as the 3rd short value in the short data section and is on a 10-bit scale like the UV Coordinates.

In general, vertex data can be extracted like so:

```C
u32_t float_data_off = data_off + 0x34,
      char_data_off  = float_data_off + ( vertex_count * 12 ) + 0x4,
      short_data_off = char_data_off  + ( vertex_count *  4 ) + 0x4,
      color_data_off = short_data_off + ( vertex_count *  8 ) + 0x4;

for(int vertex_num = 0; vertex_num < vertex_count; vertex_num++) {

    float float_data[3];
    memcpy(float_data, (void *)float_data_off, sizeof(float_data));
    float_data_off += sizeof(float_data);

    char char_data[4];
    memcpy(char_data, (void *)char_data_off, sizeof(char_data));
    char_data_off += sizeof(char_data);

    short short_data[4];
    memcpy(short_data, (void *)short_data_off, sizeof(short_data));
    short_data_off += sizeof(short_data);

    unsigned char color_data[4];
    std::memcpy(color_data, file->data(color_data_off), sizeof(color_data));
    color_data_off += sizeof(color_data);

    Vertex v = Vertex({
    v.position = {
        float_data[0],
        float_data[1],
        float_data[2]
    };
    v.normal = {
        char_data[0] / 127.0f,
        char_data[1] / 127.0f,
        char_data[2] / 127.0f
    };
    v.color = {
        fix_color(color_data[0]),
        fix_color(color_data[1]),
        fix_color(color_data[2]),
        fix_color(color_data[3])
    };
    v.uv_coordinates = {
        short_data[0] / 4096.0f,
        short_data[1] / 4096.0f
    };
    v.bone1_id = ((unsigned)short_data[3] >> 2) - 1;
    v.bone2_id = ((unsigned)char_data[3] >> 1) - 1;
    v.weight = (float)(unsigned)short_data[2] / 4096.0f;
```

## Section 2 Data

Section 2 is comprised of a list of concatenated objects. These objects or their purpose are not well researched.

0x20 byte Section 2 Objects:

|Offset|Length|Type|Function|Notes|
|-|-|-|-|-|
|0x00|16|vector4f|
|00x10|4|uint|String Offset|Does not seem to reference a file. Likely a name but not sure what for.|
|0x14|4|uint||Some sort of count|
|0x18|4|float||usually `1.0f`|
|0x1C|4|uint||usually `0`|

## Section 3 Data

Section3 is just a list of `vector4f`s representing bone positions. This data is also in the model's `.anm` file which makes this section unnecessary.

## Dictionary

All string data in the file is put into the dictionary for compact storage. It consists of `const char[]` strings with `\0` terminators concatenated together.

&nbsp;

# Animation Data Format

TODO