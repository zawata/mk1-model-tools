# Merkury Engine V1 Model Format Tools

## Model Format

For technical details, see FORMAT.md and `mdl.cpp`

## Compilation

Compilation is currently only supported on linux and windows. MacOS should work but I don't have a machine to test with.

I will try to provide binaries for major releases to ease compilation flow

Required:
 * Cmake
 * Eigen
 * Python3
 * Pybind11
 * Assimp

Windows:
 * Visual Studio

Compilation is acheived with CMake

Cmake Targets:
 * `s_mk1map`
   * The core code built as a static library
 * `pymk1map`
   * The python binding module
 * `test`
   * a testing executable
 * `mdl_cvt`
   * the MDL conversion utility

### Windows:

The easiest way to compile everything is with Visual Studio with cmake support. The community version can be found for free through microsoft

Open the project in Visual Studio. Change the build type from Debug to Release and run the build.

The build artifacts will be located at `./out/build/x64-Release/*`

Alternatiely it can be built on the command line:

```bash
mkdir build\win32
cd build\win32
cmake ../.. -DPYTHON_EXECUTABLE="C:\path\to\python.exe"
cmake --build . --config Release
```

Build artifacts will be located in `./build/unix`.

### Linux

The project builds like any other cmake project. Ninja is recommended but make works as well. Pybind may try to use `python` when building so it's safer to specify the python3 executable unless your system has python3 installed as python.

```bash
mkdir build/unix
cd build/unix
cmake .. -GNinja -DPYTHON_EXECUTABLE=$(which python3)
ninja
```

Build artifacts will be located in `./build/unix`.

## Changelog

1.0.0:
 * initial release

1.1.0:
 * Texture Aliasing
 * Vertex Colors
 * Integrate libeigen
 * Bug Fixes:
   * Exceptions no longer crash blender

2.0.0
 * Add Animation Data Processing

3.0.0
 * Model Exporting
 * removed submodules
 * new project layout
 * Added GLTF<->MDL converter
 * Abandoned the blender plugin idea.
   * I'm keeping the python binding though.